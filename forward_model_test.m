addpath('functions');
addpath('functions/GPT');
addpath('functions/crpptbx-9.2.0');
load('data/geometry_axuv.mat');
load('data/transfer_matrix.mat');
load('data/coords.mat');
load('data/vessel.mat');
mask = dlmread('data/mask.dat', ' ');
mask_highres = dlmread('data/mask_highres.dat', ' ');
load('data/shot70777emissivity.mat');
emission_at_t = shot70777emissivity.data(:,:,591)';
%emission_at_t = gaussian_dist;

%kernel = get_kernel([10000 0.05], r_coords, z_coords);

% very important to use emission tranposed to deal with Fortran ordering in
% matlab opposed to C ordering in Numpy
measurement = transfer_matrix * reshape(emission_at_t', 1875, 1);

%measurement(67:80) = -1000*ones(1, 14);

reconstruction1 = reshape(measurement' * transfer_matrix, 25, 75)';
%params = optimize_parameters(measurement, "stationary", [100000, 0.1], false, emission_at_t);
params(1)
params(2)

reconstruction2 = perform_inversion(measurement, [157000, 0.128], transfer_matrix, r_coords, z_coords, [], false);
reconstruction2(reconstruction2<0) = 0;
reconstruction2 = reconstruction2 .* flipud(mask);
figure();

subplot(4,3, 1:3)
plot(measurement);
xlabel('channel'); ylabel('signal'); title('AXUV signal via transfermatrix shot 70777 at t=0.6s')
%plot(measurement)

subplot(4,3,[4 7 10])
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, emission_at_t)
colorbar;
title('Emission')
plot(r_vessel, z_vessel, 'w');
subplot(4,3,[5 8 11])
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, reconstruction1)
title('transfermatrix * AXUV-signal')
plot(r_vessel, z_vessel, 'w');
colorbar;
subplot(4,3,[6 9 12])
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, reconstruction2)
colorbar;
title('fit via GPT')
plot(r_vessel, z_vessel, 'w');
