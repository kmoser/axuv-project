addpath("crpptbx-9.2.0/", "functions/", "functions/GPT");
load("data/coords.mat");
load("data/vessel.mat");
load("data/shot70777emissivity.mat");
load("data/shot70777axuvraw.mat");
load("data/shot70777bolopower.mat");
load("data/transfer_matrix.mat");
mask = dlmread("data/mask.dat");

time_basis_emission = shot70777emissivity.dim{3};
time_basis_axuv = shot70777axuvraw.time;
axuv_raw_data = shot70777axuvraw.data;


cal_data = calculate_calibration(   shot70777emissivity.data,   ...
                                    shot70777axuvraw.data,      ...
                                    time_basis_emission, ...
                                    time_basis_axuv,      ...
                                    transfer_matrix,       ...
                                    false);
                                
kernel = get_kernel([50000 0.1], r_coords, z_coords);
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));

inversions = zeros(size(round(time_basis_emission/100),2), 75, 25);

for i = 1:100:size(time_basis_emission)
    time_emiss = time_basis_emission(i)
    index_axuv = round((time_emiss + 0.04001)/0.000005); 
    emission_at_t = shot70777emissivity.data(:, :, i)';
    measurement = (mean(axuv_raw_data(index_axuv-3000:index_axuv+3000, :)) .* cal_data(i, :))';
    %measurement = transfer_matrix * reshape(emission_at_t', 1875, 1);
    e = perform_inversion(measurement, [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
    e(e<0) = 0;
    inversions(i, :, :) = e;
end

for i = 1:100:size(time_basis_emission)
    time_emiss = time_basis_emission(i);
    figure('Position',[10 10 700 850])
    subplot(1,2, 1);
    hold on;
    imagesc(r_coords, z_coords, shot70777emissivity.data(:, :, i)');
    axis equal; set(    gca,'YDir','Normal');
    plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
    xlabel("r in m"); ylabel("z in m");
    colorbar
    subplot(1,2, 2);
    hold on;
    imagesc(r_coords, z_coords, reshape(inversions(i, :, :), 75, 25) .* flipud(mask));
    axis equal; set(    gca,'YDir','Normal');
    plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
    xlabel("r in m"); ylabel("z in m");
    title(sprintf("70782 AXUV t=%.4f", time_emiss));
    colorbar;
    filename = sprintf('results/shot70777/emiss%05d_2.png', round(time_emiss*10000));
    saveas(gcf, filename);
    close all;
end

% figure;
% subplot(3,3, [4, 7]);
% imagesc(r_coords, z_coords, emission_at_t .* flipud(mask)); hold on;
% axis equal; set(gca,'YDir','Normal');
% colorbar;
% plot(r_vessel, z_vessel, 'w');
% 
% subplot(3,3, [5 6]);
% plot(measurement);
% title("axuv-forward")
% 
% subplot(3,3, [8 9]);
% plot(axuv_at_t);
% title("axuv-raw")
% 
% subplot(3,3, 1:3);
% plot(measurement./axuv_at_t');
% title("calibration-factor");

