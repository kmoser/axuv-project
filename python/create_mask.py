import numpy as np
import matplotlib.path as mplPath

vessel = np.genfromtxt('vessel.dat', delimiter=',')
bbPath = mplPath.Path(vessel)

r_coords = np.loadtxt("r_coords.dat")
z_coords = np.loadtxt("z_coords.dat")

r_coords = np.linspace(r_coords[0], r_coords[-1], 250)
z_coords = np.linspace(z_coords[0], z_coords[-1], 750)

mesh = np.array(np.meshgrid(r_coords, z_coords)).T
mask = np.zeros((len(z_coords), len(r_coords)), dtype=bool)

for i in range(len(z_coords)):
    for j in range(len(r_coords)):
        mask[i, j] = bbPath.contains_point((r_coords[j], z_coords[i]))
mask = mask[::-1]
mask[mask>0] = 1

np.savetxt("mask_highres.dat", mask)
