addpath("crpptbx-9.2.0/", "functions/");
load("data/coords.mat");
load("data/vessel.mat");
load("data/shot70777emissivity.mat");
load("data/shot70777axuvraw.mat");
load("data/transfer_matrix.mat");
mask = dlmread("data/mask.dat");

cal_data = calculate_calibration(   shot70777emissivity.data,   ...
                                    shot70777axuvraw.data,      ...
                                    shot70777emissivity.dim{3}, ...
                                    shot70777axuvraw.time,      ...
                                    transfer_matrix);
%cal_data = cal_data*10;
timebasis = shot70777emissivity.dim{3};       
time_axis = timebasis(1) + (1:size(cal_data, 1))*0.001;        

figure;
subplot(3, 1, 1)
imagesc(1:140, time_axis, log10(abs(cal_data)));
set(gca,'YDir','Normal');
ylabel time;
title("shot70777 log_{10} calibration Korbinian");
colorbar();
caxis([0 max(max(log10(abs(calibration.factors))))]);


subplot(2, 1, 2)
imagesc(1:140, calibration.time_calibrated, log10(abs(calibration.factors)));
set(gca,'YDir','Normal');
ylabel time;
title("shot70777 log_{10} calibration Luke");
colorbar();
caxis([0 max(max(log10(abs(calibration.factors))))]);
colorbar;
ylim([0.02 1.6])

index_emiss1 = (0.6 - 0.009)/0.001;
index_emiss2 = (0.6 - 0.0195)/0.0005;

calibration_at_t_korbi = cal_data(index_emiss1, :);
calibration_at_t_luke = calibration.factors(index_emiss2, :);

figure;
subplot(311);
plot(calibration_at_t_korbi)
title("calibration at t=0.6s Korbinian")

subplot(312);
plot(calibration_at_t_luke)
title("calibration at t=0.6s Luke")

subplot(313);
plot(calibration_at_t_luke)
ylim([0 2e6])
title("calibration at t=0.6s Luke limited")
