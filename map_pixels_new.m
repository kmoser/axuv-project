% This script computes the transfer matrix

addpath("functions");
load("data/coords.mat");
load("data/vessel.mat");
load("data/geometry_axuv.mat");
mask = dlmread("data/mask.dat");
mask_fullres = dlmread("data/mask_highres.dat");
mask100x40 = dlmread("data/mask_100x40.dat");
mask500x200 = dlmread("data/mask_highres_500x200.dat");

r_res = 40;
z_res = 100;
r_res_initial = 200;
z_res_initial = 500;

pixel_r_size = (r_coords(end) - r_coords(1))/r_res;
pixel_z_size = (z_coords(end) - z_coords(1))/z_res;
% deal with pixel offset
%pixel_r_coords = linspace(r_coords(1) - 0.5*pixel_r_size,                   ...
%                    r_coords(end) + 0.5*pixel_r_size, 25);
%pixel_z_coords = linspace(z_coords(1) - 0.5*pixel_z_size,                   ...
%                    z_coords(end) + 0.5*pixel_r_size, 75);
pixel_r_coords = linspace(r_coords(1), r_coords(end), r_res_initial);
pixel_z_coords = linspace(z_coords(1), z_coords(end), z_res_initial);

% manualy specify channels that are shadowed
has_simple_shadow = [7, 8, 134, 135, 136, 137];
has_complex_shadow = [9, 10, 13, 14, 124, 125, 132, 133];


transfer_matrix_full = zeros(140, r_res_initial*z_res_initial);
transfer_matrix = zeros(140, r_res*z_res);

for channel_number = 1:140
    
    use_simple_shadow = any(ismember(channel_number, has_simple_shadow));
    use_complex_shadow = any(ismember(channel_number, has_complex_shadow));
    
    r_detector = geometry.xdet(channel_number);
    z_detector = geometry.ydet(channel_number);
    r_slit = geometry.xap(channel_number);
    z_slit = geometry.yap(channel_number);
    r_chord = geometry.xchord(channel_number, 1:2);
    z_chord = geometry.ychord(channel_number, 1:2);
    detector_size = geometry.d1(channel_number);
    slit_size = geometry.b1(channel_number);
    distance = geometry.distance(channel_number);

    % angle different when channel starts on top/bottom or on the side
    if r_chord(1) > max(r_coords)
        chord_angle = 0.5*pi - geometry.chordangle(channel_number);
        z_slit_ends = [z_slit + 0.5*slit_size*sin(chord_angle), ...
                        z_slit - 0.5*slit_size*sin(chord_angle)];
        z_detector_ends = [z_detector + 0.5*detector_size*sin(chord_angle), ...
                        z_detector - 0.5*detector_size*sin(chord_angle)];
    else
        chord_angle = geometry.chordangle(channel_number);
        z_slit_ends = [z_slit - 0.5*slit_size*sin(chord_angle), ...
                        z_slit + 0.5*slit_size*sin(chord_angle)];
        z_detector_ends = [z_detector - 0.5*detector_size*sin(chord_angle), ...
                        z_detector + 0.5*detector_size*sin(chord_angle)];                
    end

    r_slit_ends = [r_slit - 0.5*slit_size*cos(chord_angle), ...
                        r_slit + 0.5*slit_size*cos(chord_angle)];
    r_detector_ends = [r_detector - 0.5*detector_size*cos(chord_angle), ...
                        r_detector + 0.5*detector_size*cos(chord_angle)];
    
    pixels = zeros(z_res_initial, r_res_initial);
    
    % iterate over all pixels
    for i = 1:size(pixel_r_coords, 2)
        if use_complex_shadow
            i %show progress
        end
        r = pixel_r_coords(i);
        for j = 1:size(pixel_z_coords, 2)
            z = pixel_z_coords(j);
            
            % compute slope and axis intersection between pixel 
            % and slit/aperture ends
            m1 = (z-z_slit_ends(1))/(r-r_slit_ends(1));
            m2 = (z-z_slit_ends(2))/(r-r_slit_ends(2));
            b1 = z - m1*r;
            b2 = z - m2*r;
            
            % compute slope and axis intersection of detector line
            m_det = (z_detector_ends(1)-z_detector_ends(2))/(r_detector_ends(1)-r_detector_ends(2));
            b_det = z_detector_ends(1) - m_det*r_detector_ends(1);

            % compute inerceptions of detector line and pixel-slit-lines
            r_intercept1 = (b_det-b1)/(m1-m_det);
            r_intercept2 = (b_det-b2)/(m2-m_det);
            z_intercept1 = m1*r_intercept1 + b1;
            z_intercept2 = m2*r_intercept2 + b2;

            min_d = min(r_detector_ends);
            max_d = max(r_detector_ends);

            line1_inside_detector = (max_d > r_intercept1) && (r_intercept1 > min_d);
            line2_inside_detector = (max_d > r_intercept2) && (r_intercept2 > min_d);

            is_in_sight = ~(((r_intercept1 < min_d) && (r_intercept2 < min_d)) || ...
                            ((r_intercept1 > max_d) && (r_intercept2 > max_d)));

            if line1_inside_detector
                endpoint1 = [r_intercept1, z_intercept1];
                ray1 = [r-r_intercept1 z-z_intercept1];
            else
                endpoint1 = [r_detector_ends(1), z_detector_ends(1)];
                ray1 = [r-r_detector_ends(1) z-z_detector_ends(1)];
            end

            if line2_inside_detector
                endpoint2 = [r_intercept2, z_intercept2];
                ray2 = [r-r_intercept2 z-z_intercept2];
            else
                endpoint2 = [r_detector_ends(2), z_detector_ends(2)];
                ray2 = [r-r_detector_ends(2) z-z_detector_ends(2)];
            end

            effective_angle = acos(dot(ray1, ray2)/(norm(ray1)*norm(ray2))) * is_in_sight;

            effective_contribution = effective_angle/(2*pi);
            
            if use_complex_shadow && is_in_sight
                if is_pixel_shadowed(mask500x200, [r, z], r_chord,  ...
                            z_chord, pixel_r_coords, pixel_z_coords)
                    effective_contribution = 0;
                end
            end

            pixels(j,i) = effective_contribution;
        end
    end
    
    if use_simple_shadow 
        for i = 1:z_res_initial
            if (((pixel_z_coords(i) < -0.35) && (z_chord(1) > -0.3)) || ...
                ((pixel_z_coords(i) > -0.35) && (z_chord(1) < -0.3)))
                pixels(i, 1:end) = 0;
            end
        end
    end

    % multiply with mask and downscale to desired size
    pixels = real(pixels .* flipud(mask500x200));
    pixels_downscaled = downscale(pixels, 5) .* flipud(mask100x40);
    
    transfer_matrix_full(channel_number, :) = reshape(pixels, z_res_initial*r_res_initial, 1);
    transfer_matrix(channel_number, :) = reshape(pixels_downscaled, z_res*r_res, 1);
    
%     figure('Position',[10 10 300 850])
%     hold on;
%     imagesc(r_coords, z_coords, pixels_downscaled);
%     axis equal; set(    gca,'YDir','Normal');
%     plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
%     xlabel("r in m"); ylabel("z in m");
%     title(sprintf("AXUV channel %d", channel_number));
%     colorbar;
%     filename = sprintf('results/channel_images/alternative100X40resolution/channel%03d.png', channel_number)
%     saveas(gcf, filename);
%     close all;
%     
%     figure('Position',[10 10 300 850])
%     hold on;
%     imagesc(r_coords, z_coords, pixels);
%     axis equal; set(    gca,'YDir','Normal');
%     plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
%     xlabel("r in m"); ylabel("z in m");
%     title(sprintf("AXUV channel %d", channel_number));
%     colorbar;
%     filename = sprintf('results/channel_images/alternative100X40resolution/initial_resolution/channel%03d.png', channel_number);
%     saveas(gcf, filename);
%     close all;
    
end

% IMPORTANT!!!
% do this to fix the ordering (C ordering in Python vs Fortran ordering in Matlab)
transfer_matrix = reshape(transfer_matrix, 140, z_res, r_res);
transfer_matrix = permute(transfer_matrix, [1, 3, 2]);
transfer_matrix = reshape(transfer_matrix, 140, r_res*z_res);

transfer_matrix_full = reshape(transfer_matrix_full, 140, z_res_initial, r_res_initial);
transfer_matrix_full = permute(transfer_matrix_full, [1, 3, 2]);
transfer_matrix_full = reshape(transfer_matrix_full, 140, z_res_initial*r_res_initial);

%hold on;
%scatter([r_slit_ends, r_detector_ends], [z_slit_ends, z_detector_ends])                
%plot(r_chord, z_chord)
%t = reshape(transfer_matrix_uncal_const(channel_number, :), 75, 25);
%imagesc(pixel_r_coords, pixel_z_coords, result)
%line(r_detector_ends, z_detector_ends, "color", "red")
%scatter(r_detector_ends, z_detector_ends)

%line(r_slit_ends, z_slit_ends, "color", "green")
%scatter(r_slit_ends, z_slit_ends)

%line([r endpoint1(1)], [z endpoint1(2)], "color", "magenta")
%scatter([r endpoint1(1)], [z endpoint1(2)])

%line([r endpoint2(1)], [z endpoint2(2)], "color", "magenta")
%scatter([r endpoint2(1)], [z endpoint2(2)])

%axis equal; set(gca,'YDir','Normal');
%colorbar;
%plot([r_detector, r_slit], [z_detector, z_slit])
