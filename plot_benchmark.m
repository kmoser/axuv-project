addpath('functions/crpptbx-9.2.0/', 'functions/', 'functions/GPT');
load('data/coords.mat');
load('data/vessel.mat');
load('data/geometry_axuv.mat');
load('data/shot70777emissivity.mat');
load('data/shot70777axuvraw.mat');
load('data/transfer_matrix.mat');
mask = dlmread('data/mask.dat');

shot = 70777;
emission_at_t = shot70777emissivity.data(:,:,591)';

[zi, ri] = find(emission_at_t == max(max(emission_at_t)));

r_max = r_coords(ri);
z_max = z_coords(zi);

measurement = transfer_matrix * reshape(emission_at_t', 1875, 1);
%params = optimize_parameters(measurement, 'stationary', [100000, 0.1], false, [])
reconstruction1 = perform_inversion(measurement, params, transfer_matrix, r_coords, z_coords, [], false);
reconstruction1(reconstruction1<0) = 0;
reconstruction1 = reconstruction1 .* flipud(mask);


%params_non_stat = optimize_parameters(measurement, 'non_stationary', [170000, 0.08, 0.07], false, emission_at_t)
kernel = get_non_stationary_kernel(params_non_stat, emission_at_t, r_coords, z_coords);
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
reconstruction2 = perform_inversion(measurement, [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
reconstruction2(reconstruction2<0) = 0;
reconstruction2 = reconstruction2 .* flipud(mask);

reconstruction3 = gti_fisher(measurement, 0.01, transfer_matrix, 70777, 0.6, geometry);
reconstruction3(reconstruction3<0) = 0;
reconstruction3 = reconstruction3 .* flipud(mask);


nrmse1 = norm(reconstruction1 - emission_at_t)/norm(emission_at_t)
nrmse2 = norm(reconstruction2 - emission_at_t)/norm(emission_at_t)
nrmse3 = norm(reconstruction3 - emission_at_t)/norm(emission_at_t)

psnr1 = psnr(reconstruction1, emission_at_t)
psnr2 = psnr(reconstruction2, emission_at_t)
psnr3 = psnr(reconstruction3, emission_at_t)

ssim1 = ssim(reconstruction1, emission_at_t)
ssim2 = ssim(reconstruction2, emission_at_t)
ssim3 = ssim(reconstruction3, emission_at_t)

rdte1 = (sum(sum(2*pi*r_coords .* reconstruction1')) - sum(sum(2*pi*r_coords .* emission_at_t')))/ ...
            sum(sum(2*pi*r_coords .* emission_at_t'))
rdte2 = (sum(sum(2*pi*r_coords .* reconstruction2')) - sum(sum(2*pi*r_coords .* emission_at_t')))/ ...
            sum(sum(2*pi*r_coords .* emission_at_t'))
rdte3 = (sum(sum(2*pi*r_coords .* reconstruction3')) - sum(sum(2*pi*r_coords .* emission_at_t')))/ ...
            sum(sum(2*pi*r_coords .* emission_at_t'))

        
        
hold on;
scatter(1:140, transfer_matrix * reshape(reconstruction1', 1875, 1), 'r*')
scatter(1:140, transfer_matrix * reshape(reconstruction2', 1875, 1), 'bx')
scatter(1:140, transfer_matrix * reshape(reconstruction3', 1875, 1), 'go')
errorbar(1:140, measurement, 0.05*measurement, 'k.')
legend('fit GPT stationary', "fit GPT non-stationary", "fit minimum fisher", 'forward model with error')
ylabel("AXUV-signal [AU]")
xlabel("channel")
title("Shot 70777 at t=0.6s")
% max_emiss = max(max(emission_at_t));
% 
% color = [0.75 0.75 0.75];
% 
% subplot(141)
% hold on;
% axis equal; set(gca,'YDir','Normal');
% imagesc(r_coords, z_coords, emission_at_t)
% tcv_polview('vtTPp',shot,shot70777emissivity.dim{3}(591),'Color_p',color,'Color_b',color,'Color_P',color);
% c = colorbar;
% c.Label.String = ("W/m³");
% xlabel("R in m");
% ylabel("z in m");
% title({'bolo inversion'})
% plot(r_vessel, z_vessel, 'w');
% xlim([min(r_coords) max(r_coords)])
% ylim([min(z_coords) max(z_coords)])
% 
% subplot(142)
% hold on;
% axis equal; set(gca,'YDir','Normal');
% imagesc(r_coords, z_coords, reconstruction1)
% tcv_polview('vtTPp',shot,shot70777emissivity.dim{3}(591),'Color_p',color,'Color_b',color,'Color_P',color);
% c = colorbar;
% c.Label.String = ("W/m³");
% caxis([0, max_emiss]);
% xlabel("R in m");
% ylabel("z in m");
% title({'fit stationary'})
% plot(r_vessel, z_vessel, 'w');
% xlim([min(r_coords) max(r_coords)])
% ylim([min(z_coords) max(z_coords)])
% 
% subplot(143)
% hold on;
% axis equal; set(gca,'YDir','Normal');
% imagesc(r_coords, z_coords, reconstruction2)
% tcv_polview('vtTPp',shot,shot70777emissivity.dim{3}(591),'Color_p',color,'Color_b',color,'Color_P',color);
% c = colorbar;
% c.Label.String = ("W/m³");
% caxis([0, max_emiss]);
% xlabel("R in m");
% ylabel("z in m");
% title({'fit non-stationary'})
% plot(r_vessel, z_vessel, 'w');
% xlim([min(r_coords) max(r_coords)])
% ylim([min(z_coords) max(z_coords)])
% 
% subplot(144)
% hold on;
% axis equal; set(gca,'YDir','Normal');
% imagesc(r_coords, z_coords, reconstruction3)
% tcv_polview('vtTPp',shot,shot70777emissivity.dim{3}(591),'Color_p',color,'Color_b',color,'Color_P',color);
% c = colorbar;
% c.Label.String = ("W/m³");
% caxis([0, max_emiss]);
% xlabel("R in m");
% ylabel("z in m");
% title({'fit minimum fisher'})
% plot(r_vessel, z_vessel, 'w');
% xlim([min(r_coords) max(r_coords)])
% ylim([min(z_coords) max(z_coords)])
