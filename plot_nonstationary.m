addpath('functions/crpptbx-9.2.0/', 'functions/', 'functions/GPT');
load('data/coords.mat');
load('data/vessel.mat');
load('data/geometry_axuv.mat');
load('data/shot70777emissivity.mat');
load('data/shot70777axuvraw.mat');
load('data/transfer_matrix.mat');
mask = dlmread('data/mask.dat');

shot = 70777;
emission_at_t = shot70777emissivity.data(:,:,591)';

[zi, ri] = find(emission_at_t == max(max(emission_at_t)));

r_max = r_coords(ri);
z_max = z_coords(zi);

measurement = transfer_matrix * reshape(emission_at_t', 1875, 1);

reconstruction1 = perform_inversion(measurement, [157000, 0.128], transfer_matrix, r_coords, z_coords, [], false);
reconstruction1(reconstruction1<0) = 0;
reconstruction1 = reconstruction1 .* flipud(mask);

kernel = get_non_stationary_kernel([157000, 0.05, 0.15], emission_at_t, r_coords, z_coords);

kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
reconstruction2 = perform_inversion(measurement, [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
reconstruction2(reconstruction2<0) = 0;
reconstruction2 = reconstruction2 .* flipud(mask);

local_lengthscale = get_local_lengthscale([0.05, 0.15], r_max, z_max, r_coords, z_coords);

max_emiss = max(max(emission_at_t));

color = [0.75 0.75 0.75];

subplot(141)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, emission_at_t)
tcv_polview('vtTPp',shot,shot70777emissivity.dim{3}(591),'Color_p',color,'Color_b',color,'Color_P',color);
c = colorbar;
c.Label.String = ("W/m³");
xlabel("R in m");
ylabel("z in m");
title({'bolo inversion'})
plot(r_vessel, z_vessel, 'w');
scatter(r_max, z_max, 'r', 'filled')
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot(142)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, reconstruction1)
tcv_polview('vtTPp',shot,shot70777emissivity.dim{3}(591),'Color_p',color,'Color_b',color,'Color_P',color);
c = colorbar;
c.Label.String = ("W/m³");
caxis([0, max_emiss]);
xlabel("R in m");
ylabel("z in m");
title({'fit stationary'})
plot(r_vessel, z_vessel, 'w');
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot(143)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, reconstruction2)
tcv_polview('vtTPp',shot,shot70777emissivity.dim{3}(591),'Color_p',color,'Color_b',color,'Color_P',color);
c = colorbar;
c.Label.String = ("W/m³");
caxis([0, max_emiss]);
xlabel("R in m");
ylabel("z in m");
title({'fit non-stationary'})
plot(r_vessel, z_vessel, 'w');
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot(144)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, local_lengthscale * 100)
c = colorbar;
c.Label.String = ("cm");
xlabel("R in m");
ylabel("z in m");
title({'local lengthscale'})
plot(r_vessel, z_vessel, 'black');
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])
