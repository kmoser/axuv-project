addpath('functions/crpptbx-9.2.0/');
addpath('functions/', 'functions/GPT/');
load('data/vessel.mat');
load('data/coords.mat');
load('data/geometry_axuv.mat');
%load('data/shot71158emissivity.mat');
%load('data/shot71158axuvraw.mat');
load('data/transfer_matrix.mat');
mask = dlmread('data/mask.dat');

% shot=71158;
% mdsopen(shot);
% shot71158emissivity=tdi('\results::bolo_u:emissivity'); % Get the data
% size(shot71158emissivity.data)
% mdsclose(shot);
% shot71158axuvraw = load_diodes(shot, "diag_name", "axuv");

time_basis_emission = shot71158emissivity.dim{3};
time_basis_axuv = shot71158axuvraw.time;
axuv_raw_data = shot71158axuvraw.data;

cal_data = calculate_calibration(   shot71158emissivity.data,   ...
                                    axuv_raw_data,      ...
                                    time_basis_emission, ...
                                    time_basis_axuv,      ...
                                    transfer_matrix,        ...
                                    false);
                                
cal_data_at_t = cal_data(1322, :);

indicies_axuv = 312070:312230;
%indicies_axuv = 312100:312200;

kernel = get_kernel([300000 0.12], r_coords, z_coords);
kernel = kernel + 0.001*diag(diag(kernel));
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));

inversions = zeros(size(indicies_axuv,2), 75, 25);
inversions_var_l = zeros(size(indicies_axuv,2), 75, 25);
measurements = zeros(size(indicies_axuv,2), 140);

for i = 1:size(indicies_axuv, 2)
    time_index = indicies_axuv(i)
    time = time_basis_axuv(time_index)
    measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
    measurements(i, :) = measurement;
    %e = gti_fisher(measurement', 0.05, transfer_matrix, 71157, time, geometry);
    e = perform_inversion(measurement', [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
    e(e<0) = 0;
    inversions(i, :, :) = e;
end

for i = 1:size(indicies_axuv, 2)
    time_index = indicies_axuv(i)
    time = time_basis_axuv(time_index)
    measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
    kernel = get_non_stationary_kernel([300000, 0.08, 0.08], reshape(inversions(i, :, :), 75, 25), r_coords, z_coords);
    kernel = kernel + 0.001*diag(diag(kernel));
    kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
    e = perform_inversion(measurement', [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
    e(e<0) = 0;
    inversions_var_l(i, :, :) = e;
end

% for i = 1:size(indicies_axuv, 2)
%     time_index = indicies_axuv(i)
%     time = time_basis_axuv(time_index)
%     
%     subplot(121)
%     hold on;
%     clims = [0 max(max(max(inversions)))];
%     imagesc(r_coords, z_coords, reshape(inversions(i, :, :), 75, 25) .* flipud(mask), clims);
%     axis equal; set(    gca,'YDir','Normal');
%     plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
%     xlabel('r in m'); ylabel('z in m');
%     title(sprintf('71158 AXUV t=%.4f', time));
%     colorbar;
%     xlim([min(r_coords) max(r_coords)])
%     ylim([min(z_coords) max(z_coords)])
%     subplot(122)
%     hold on;
%     imagesc(1:140, time_basis_axuv(indicies_axuv, :), axuv_raw_data(indicies_axuv, :))
%     plot(1:140, repelem(time, 140), 'color', 'r')
%     filename = sprintf('results/shot71158smallelm/t=152GPT/emiss%7d.png', round(time*1000000));
%     saveas(gcf, filename);
%     close all;
% end


for i = 1:size(indicies_axuv, 2)
    time_index = indicies_axuv(i)
    time = time_basis_axuv(time_index)
    subplot(121)
    hold on;
    clims = [0 max(max(max(inversions_var_l)))];
    imagesc(r_coords, z_coords, reshape(inversions_var_l(i, :, :), 75, 25) .* flipud(mask), clims);
    axis equal; set(    gca,'YDir','Normal');
    plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
    xlabel('r in m'); ylabel('z in m');
    title(sprintf('71158 AXUV t=%.4f', time));
    colorbar;
    xlim([min(r_coords) max(r_coords)])
    ylim([min(z_coords) max(z_coords)])
    tcv_polview('vtTPp',71158,time, ...
               'Color_p',[0.7 0.7 0.7], 'Color_b',[0.7 0.7 0.7], ...
               'Color_P',[0.7 0.7 0.7]);
    subplot(122)
    hold on;
    imagesc(1:140, time_basis_axuv(indicies_axuv, :), axuv_raw_data(indicies_axuv, :))
    plot(1:140, repelem(time, 140), 'color', 'r')
    filename = sprintf('results/shot71158smallelm/t=1520GPT_var_l/emiss%7d.png', round(time*1000000));
    saveas(gcf, filename);
    close all;
end