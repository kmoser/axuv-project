addpath('functions/crpptbx-9.2.0/', 'functions/', 'functions/GPT');
load('data/coords.mat');
load('data/vessel.mat');
load('data/geometry_axuv.mat');
load('data/shot70777emissivity.mat');
load('data/shot70777axuvraw.mat');
load('data/transfer_matrix.mat');
mask = dlmread('data/mask.dat');

emission_at_t = shot70777emissivity.data(:,:,591)';

measurement = transfer_matrix * reshape(emission_at_t', 1875, 1);
%params = optimize_parameters(measurement, [10000, 0.1], false);

[reconstruction1, post_std] = perform_inversion(measurement, [153000, 0.128], transfer_matrix, r_coords, z_coords, [], false);
reconstruction1(reconstruction1<0) = 0;
reconstruction1 = reconstruction1 .* flipud(mask);

post_std = reshape(post_std, 25, 75)';

% reconstruction2 = gti_fisher(measurement, 0.004, transfer_matrix, 70777, 0.6, geometry);
% reconstruction3 = gti_fisher(measurement, 0.04, [], 70777, 0.6, geometry);

color = [0.75 0.75 0.75];

subplot(121)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, post_std .* flipud(mask))
tcv_polview('vtTPp',shot,shot70777emissivity.dim{3}(591),'Color_p',color,'Color_b',color,'Color_P',color);
c = colorbar;
c.Label.String = ("W/m³");
xlabel("R in m");
ylabel("z in m");
title("standard deviation")
plot(r_vessel, z_vessel, 'w');
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

data2 =  abs(reconstruction1 - emission_at_t)./post_std;

subplot(122)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, data2)
tcv_polview('vtTPp',shot,shot70777emissivity.dim{3}(591),'Color_p',color,'Color_b',color,'Color_P',color);
c = colorbar;
caxis([0, max(max(data2))]);
xlabel("R in m");
ylabel("z in m");
title("|sample-fit|/\sigma")
plot(r_vessel, z_vessel, 'w');
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])


