\section{Forward Model for RADCAMs AXUV Channels}

\subsection{Mathematical Description of Forward Model}

Mathematically a single AXUV channel performs an integration over the 
continuous emissivity function \(E(\vec{r})\), describing the emission 
distribution within the vessel
\begin{equation}\label{kap3_eq_sightline_integral}
    P_i = \int S_i(\vec{r})E(\vec{r})\mathrm{d^3r} + \varepsilon_i
\end{equation}
Here \(S_i\) is the response function for the \(i\)-th AXUV channel that describes 
the viewing cone or how strong an infinitesimal volume element contributes to the 
channel and \(\varepsilon_i\) it the error of the channel. \(P_i\) is the measured power
of the \(i\)-th AXUV channel. By switching to a discrete emission distribution
over \(n\) pixels the measurement process for all \(m\) channels can be 
written as a matrix multiplication, that will be called the forward model from here on,
\begin{equation}\label{eq_forward_model}
    \vec{P} = \mathbf{S} \cdot \vec{E} + \vec{\varepsilon}
\end{equation} 
where \(\vec{\varepsilon}\) is the measurement error and 
\(\mathbf{S}\) is the transfer matrix of size \(m\times n\), mapping how much each of 
the \(n\) pixels contributes to the sight cones of the \(m\) AXUV channels.


\subsection{Computation of Transfer Matrix for AXUV Channels}

A single AXUV channel consist of a detector and a slit/aperture in front of it. 
The geometry of the channel is then defined by the absolute position of the 
detector and aperture within the vessel and their size and orientation. In the 
following we treat the geometry only in the two dimensional toroidal cross section. 
Figure \ref{img_axuv_channel_geo} shows the geometry for one channel in principle. 
Instead of explicitly integrating, the contribution is computed for each pixel and 
the contribution within the pixel towards the channels measurement is regarded as 
uniform.

The contribution of each pixel is calculated by computing the intersection between 
the pixel and the detector that is visible through the aperture. The assumption 
is that emission within the plasma propagates uniformly and in one particular 
direction. The power is then uniformly distributed across \(2\pi\) or 360°.
The fraction of power that reaches the detector from a single pixel is just 
given by the opening angle of the viewing cone describing how much of the detector is 
visible from the pixels position divided by \(2\pi\). This is visualized in figure 
\ref{img_axuv_channel_geo_pixel}. By doing this for each pixel and for each channel 
a \(m\times n\) matrix is obtained where \(m\) is the number channels and \(n\) the 
number of pixels.

In order to deal with pixels that are partially within the viewing cone of the channel 
the computation of the transfer matrix is initially done for a higher resolution and 
then scaled down to the target resolution afterwards. 
The transfer matricies were computed for resolutions of 750x250 and 500x200 pixels 
and then scaled down to 75x25 and 100x40 pixels which are common resolutions for 
tomograms at TCV.

For some channels the line of sight for some pixels is obstructed by structures 
within the vessel. An example for a partially blocked channel is shown in figure 
\ref{img_axuv_channel10}. In cases where channels are completely blocked, their 
entries in the transfermatrix are simply set to zero below/above the z-coordinate of 
the obstruction. For cases like in figure \ref{img_axuv_channel10} a more sophisticated 
approach is needed. Here for each pixel within the viewing cone the closest pixels 
along the central line of sight from the pixel to the aperture are identified. 
The pixel is regarded as shadowed and its entry in the transfer matrix is set to 0,
if any of those pixels along the line of sight is within an obstruction represented by 
a binary mask with the same resolution as the transfer matrix. This essentially 
results in iteration over all pixels for all pixels within the viewing cone which has 
a very poor performance. The computation for one partially obstructed channel therefore 
can take several minutes. There are certainly solutions solving the same problem 
orders of magnitude faster, but since the transfer matrix does not have to be 
calculated regularly, no time was spent in optimizing the algorithm.

In order to get absolute power values for each channel the relative contribution 
of each pixel would have to be integrated/multiplied with the size of the pixel 
in the toroidal plane and an integration over the viewing cone in the toroidal dimension 
would have to be carried out. Since the width of the viewing cone increases with 
distance from the aperture but the etendue decreases it would boil down to a 
multiplicative factor for the whole transfer matrix. For the purpose of this paper 
this is not relevant since the transfer matrix is both used for computing 
measurements/calibration factors and for the tomography. Hence, that factor simply 
cancels out. It is still important to mention that the transfer matrix does not 
provide absolute power values in a meaningful unit yet. 


\begin{figure}
    \centering
    \includegraphics[width=0.75\linewidth]{axuv-geometry.png}
    \caption{Visualization of the geometry of a single AXUV channel. Pixels within 
    the core region can see the whole detector, while pixels outside of it either 
    only partially see the detector leading to a reduced contribution to the 
    channel measurement or are completely out of sight leading to no contribution.}
    \label{img_axuv_channel_geo}
\end{figure}


\begin{figure}
\centering
\begin{subfigure}{.6\linewidth}
    \centering
    \includegraphics[width=\linewidth]{axuv-geometry_pixel.png}
    \subcaption{}
    \label{img_axuv_channel_geo_pixel}
\end{subfigure}
\begin{subfigure}{.3\linewidth}
    \centering
    \includegraphics[width=\linewidth]{channel010.png}
    \subcaption{}
    \label{img_axuv_channel10}
\end{subfigure}
\caption{\textbf{a)} Depiction of the geometry from the viewpoint of a single pixel. This 
    specific pixel would be in the edge region as it only has partial view of the 
    detector. \textbf{b)} Relative contribution of each pixel for AXUV channel 10. This specific 
    channel is also partially shadowed by structures within the vessel.}
\end{figure}
