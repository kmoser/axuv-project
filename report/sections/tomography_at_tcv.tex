\section{AXUV Tomography for RADCAM}

\subsection{Implementation of GPT}

The code for GPT was ported from \cite{myGPTpaper} from Python to Matlab for 
this work. For a given measurement first the kernel matrix \(\mathbf{\Sigma}_E\)
is computed and then the fitted tomogram is obtained via Eq. \ref{eq_mean_post}. 
For \(\mathbf{\Sigma}_P\) a noise level of 5\% was assumed.
Since the Gaussian process is not aware of the vessel boundaries it is possible 
that non zero emissivity outside of the vessel exists. The entries in the 
transfer matrix are zero outside of the vessel which makes the posterior emissivity 
go to zero quickly outside of the vessel, but especially with larger length scales 
in the kernel matrix this might not always happen fast enough. Hence the result 
is multiplied with a binary mask representing the vessel shape. Similarly 
emissivity being modeled by Gaussian distributions have no upper and lower 
boundaries and can therefore become negative. Usually only a few pixels end up 
with minor negative values. Negative values are simply set to 0  
after the fit is calculated. This increases the total integrated emission 
slightly, but the error from this should be small compared to the total emission 
in most cases. Another way of dealing with this would be to rescale the emissivity 
after removing negative values, so that the integrated emission does not change. 
This would lower the amplitudes of the emissivity slightly.
step. Figure \ref{img_hyperparam_effect} shows the effect of choosing different 
hyperparameters for an example.


\subsubsection{Stationary Kernel Function}

A very simple but reasonable choice for a kernel function is a squared exponential.
There the corelation between pixels is assumed to drop off exponentially with 
increasing distance between pixels. The kernel matrix is simply computed by 
evaluating the kernel function, here a squared exponential, for every pixel pair.
\begin{equation}\label{eq_stat_kernel}
    \Sigma_E^{ij} = k_{SE}(\vec{r}_i, \vec{r}_j) = \sigma_E^2\exp
    \left(-\frac{(\vec{r}_i-\vec{r}_j)^2}{2l^2}\right)
\end{equation}
Here \(\vec{r}_i\) and \(\vec{r}_j\) are the R- and z-coordinates of 
the i-th and j-th pixel. This kernel
function has two hyperparameters; \(\sigma_E\) describing the standard deviation 
of the prior distribution or to what extent the amplitude in the fitted emissivity 
is changing and \(l\) the length scale or corelation length that specifies how 
quickly the corelation between pixels drops of. Larger length scales result in 
smoother fits with less local features. Choosing smaller length scales has the 
opposite effect. As a rule of thumb \(\sigma_E\) should be a bit smaller but around 
the same order of magnitude as the maximum value of the fitted emissivity (which of course is 
not necessarily known beforehand) and \(l\) should be bigger than the pixel size
(\(\approx\) 2 cm for 75x25 resolution) but also quite a bit smaller than the 
vessel size. In section \ref{section_optimization} a method for finding the 
best parameters for a given measurement is shown. 
With the selection of the length scale a tradeoff has to be made between resolving 
finer structures in areas with very localized emission events like the divertor or 
separatrix and getting a smoother distribution elsewhere.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{hyperparam_effect.jpg}
    \caption{Fit for same measurements with different parameters. While \(l\)
    mainly defines how fast the emissivity varies spatially, \(\sigma_E\) defines 
    how strong the amplitude of the emissivity varies.}
    \label{img_hyperparam_effect}
\end{figure}


\subsubsection{Non-Stationary Kernel Function}

The previous kernel function is stationary in the sense that its values do not 
depend on individual pixels but only on the distance between two pixels. In order 
get a better fit it is possible to assign different length scales to different pixels. 
Such a general non stationary squared exponential kernel would take the following 
form
\begin{equation}\label{eq_non_stat_kernel}
    k_{\text{var\_l}}(\vec{r}_i, \vec{r}_j) = \sigma_{\mathrm{var\_l}}^2\exp
    \left(-\frac{(\vec{r}_i-\vec{r}_j)^2}{l(\vec{r}_i)^2 + l(\vec{r}_j)^2}\right)
\end{equation}
where \(l(\vec{r})\) is a function that describes the length scale for a given 
pixel.

Since the plasma shape of TCV is highly flexible finding a good general \(l(\vec{r})\)
is not easy. At ASDEX Upgrade for example the divertor region was assigned 
a lower length scale and the length scale was then modeled to incease along the 
z-axis via \(\tanh\)-function \cite{myGPTpaper}. For TCV one proposed way would be to 
identify a region of interest via the brightest pixel either from a bolometer inversion 
or if not available from a fit via the stationary kernel. This area would then 
be assigned a lower length scale and the length scale would increase with 
distance via a \(\tanh\)-function in both the r and z dimension
\begin{equation}
    l(\vec{r}) = l_0 + \delta l\left[1+\tanh\left(\frac{(\vec{r} - \vec{r}_0)^2-r_{\text{offset}}}
    {\delta r}\right)\right]
\end{equation}
Therefore, \(k_{\text{var\_l}}\) has 
three hyperparameters \(\sigma_{\text{var\_l}}\), \(l_0\) and \(\delta l\).
\(l_0\) is the base length scale in the area of interest and \(\delta l\)
the part that gets added to the length scale in other regions. 
\(\sigma_{\text{var\_l}}\) is again the standard deviation of the prior distribution. 
\(\vec{r}_0\) is 
the position/center of the area of interest and \(r_{\text{offset}}\) defines 
at what distance from it the transition to he higher length scale occurs. Lastly 
\(\delta r\) determines how fast the transition happens. Both \(r_{\text{offset}}\) 
and \(\delta r\) were fixed to 22 cm and 12 cm.
An example of the local length scale is shown in figure \ref{img_non_stationary}.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{non_stationary.png}
    \caption{The left plot shows the existing bolometer inversion for shot 70777 
    at \(t=0.6\) s. From this the AXUV-signal was computed via the forward model. 
    This is used as both the input for the GPT with the stationary and non-stationary
    kernel. The results from this are shown in the middle. For the stationary kernel 
    the parameters were obtained via optimization of the marginal log likelihood 
    as described in \ref{section_optimization}. For the non-stationary kernel 
    the parameters were chosen by hand (\(l_0 = 5\) cm and \(\delta l = 15\) cm) 
    to highlight the effect of the non-stationary kernel. There the fit around the X-point 
    is more localized and has a higher amplitude which is closer to the bolometer 
    inversion while having a slightly smoother distribution elsewhere. On the 
    right the local length scale for this specific example is shown.}
    \label{img_non_stationary}
\end{figure}


\subsubsection{Optimization of Hyperparameters}\label{section_optimization}

A common way of optimizing hyperparameters for Gaussian process regression is by 
maximizing the logarithm of the marginal likelihood \cite{Rasmussen06}. In other 
words the probability of getting the measurement for given parameters is optimized.
In our case the marginal log likelihood takes the following form \cite{bachelor}

\begin{equation}\label{kap3_eq_log_like}
    \ln(p(\vec{P}|\vec{\theta})) = 
    - \frac{1}{2}\ln(\det[\mathbf{\Sigma}_P + \mathbf{S\Sigma}_E\mathbf{S}^\mathrm{T}]) 
    - \frac{1}{2}(\vec{P}-\mathbf{S}\vec{\mu}_E)^\mathrm{T}[\mathbf{\Sigma}_P + 
    \mathbf{S\Sigma}_E\mathbf{S}^\mathrm{T}]^{-1}(\vec{P}-\mathbf{S}\vec{\mu}_E)
\end{equation}

\(\vec{\mu}_E\) is assumed to be 0 in our case. By maximizing this function with 
regard to the hyperparameters which are contained in the kernel function 
\(\mathbf{\Sigma}_E\) we get the most likely parameters.

The function is optimized with the \textit{fminsearch} function of Matlab which 
finds the mimimum of unconstrained multivariate functions without relying on 
derivatives. It is based on the Nelder-Mead-algorithm
\footnote{\url{https://ch.mathworks.com/help/matlab/ref/fminsearch.html}}.

It should be noted that optimizing the parameters is only practicable for 
small datasets of single time points. Especially for more complex kernel 
functions and cases where the measurements are of bad quality (faulty channels, 
saturated channels, drifting channels etc.) it might not converge or return 
bad parameters.

More details can be found in \cite{bachelor, Rasmussen06}.


\subsection{Results and Comparison with Minimum Fisher Tomography}

As a benchmark for comparing the tomography methods shot 70777 at \(t = 0.6\) s
was chosen. From the existing bolometer inversion obtained via minimum fisher
information the AXUV-signal is calculated via the forward model. Then fits for the 
stationary and non-stationary kernel are computed with optimized hyperparameters. 
Additionally a fit is computed via minimum fisher information with the previously 
obtained AXUV-geometry and \(0.01\) as the Siga parameter. For comparison different 
metrics are computed for the fits comparing them to the initial bolometer inversion.
The first metric is the normalized root mean squared error (NRMSE) between the bolometer 
inversion and the fit. The second is the peak signal to noise ratio (PSNR), which is 
commonly used for comparing compressed images \cite{PSNRjpg}. A higher value there would indicate 
a better fit. The structural similarity index (SSIM) is a more sophisticated metric 
used for comparing the similarity of different images. It quantifies the perceived differences 
between images by humans. It ranges from 1 which would indicated exact copies to 
0 for completely different images \cite{SSIMpaper}. Lastly the relative error in total 
emission (RDTE) between the bolometer inversion and the fit is computed. The bolometer inversion
and the fits for the different methods are shown in figure \ref{img_benchmark}. 
Table \ref{tab_benchmark} shows the results for the aforementioned metrics. In order 
to get conclusive results the metrics would have to be calculated for more than one 
data point and for different emissivities. The existing data inidicates that the fit quality 
of all three methods is comparable. In this specific example the non-stationary kernel 
does not show significant improvements over the simpler stationary kernel. 
The fit via fisher information has a higher SSIM which can also be verified by looking 
at figure \ref{img_benchmark} where the fit via fisher information is a closer resemblance to 
the bolometer inversion compared to the fits via GPT. This is to be expected since the initial 
bolometer inversion is also computed via fisher information and might not necessarily 
indicate a better quality of the fit. 

Another way of comparing whether the fits are compatible with the input data is via 
the forward model. Figure \ref{img_forward_comparison} shows the measurement computed 
from the bolometer inversion that is used as input to the different tomography methods
together with the assumed noise level of 5\%. The signal obtained by applying the 
forward model to the three different fits is also plotted. In all three cases 
the signal is within one standard deviation of the input signal for most channels 
showing that the fits are compatible with the bolometer inversion.

One advantage of GPT is that one not only obtains a fit (the expectation value of the 
posterior emission distribution) but also the error of the fit in form of the variance/standard 
deviation of the posterior distribution. The variance is simply obtained via the diagonal 
values of the posterior covariance matrix (Eq. \ref{eq_mean_cov}). Figure \ref{img_post_std} shows 
the standard deviation of the fit via the stationary kernel on the left. The right plot 
shows the absolute error between the bolometer inversion and the fit for the AXUV-diodes
divided by the standard deviation of the fit. While a few pixels at the vessel boundaries and a 
single pixel in the vicinity of the X-point have deviations of around 6 standard deviations 
the majority of pixels has significantly smaller values. The mean value of the pixels 
within the vessel is around 0.76 standard deviations. This shows that the fit 
for the AXUV-diodes via GPT is mostly compatible with the bolometer inversion. 
The larger error close to the vessel boundaries is due to the fact that the 
transfer matrix is 0 outside of the vessel,
which results in the emissivity quickly dropping 0 outside of the vessel and a 
larger uncertainty.

Another difference between the GPT method and the tomography method based on minimum fisher 
information is that while the later relies on an iterative approach the former is 
purely analytical. This results in relatively constant time requirements for each calculation.
When the parameters/kernel is fixed for a specific shot and the error is assumed constant or rather 
not depending on the signal strength and thus being time dependent, then the time 
consuming matrix inversions in Eq. \ref{eq_mean_post} and \ref{eq_mean_cov} only 
have to be carried out once 
and calculating fits boils down to simple matrix multiplication. The time for one computation 
should then be in the range of milliseconds \cite{myGPTpaper}. This in combination 
with the constant time requirement enables realtime capability.



\begin{table}
    \centering
    \begin{tabular}{l c c c c}\toprule
        & NRMSE\(\downarrow\) & PSNR\(\uparrow\) & SSIM\(\uparrow\) &  RDTE\(\downarrow\)\\ \midrule
        fit stationary & 0.142 & 91.0 & 0.631 & 1.96\%\\
        fit non-stationary & 0.151 & 91.4 & 0.630 & 2.32\%\\
        fit minimum fisher & 0.127 & 89.3 & 0.717 & 3.44\%\\
        \bottomrule
    \end{tabular}
    \caption{Different metrics for all three fits in figure \ref{img_benchmark} compared 
    to the bolometer inversion. The arrows indicate whether smaller or bigger numbers 
    are better.}
    \label{tab_benchmark}
\end{table}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{benchmark.png}
    \caption{Comparion of fits obtained via minimum fisher information and Gaussian 
    process tomography for shot 70777 at \(t=0.6\) s.}
    \label{img_benchmark}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{forward_comparison.png}
    \caption{The black data points with error bars show the 
    AXUV-signal that was computed from the bolometer inversion 
    with the forward model. When applying the forward model 
    to the three different fits they all are within the 
    error for most channels.}
    \label{img_forward_comparison}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.65\linewidth]{post_std.png}
    \caption{The left plot show the standard deviation of the fitted emissivity 
    via GPT and the stationary kernel for shot 70777 at \(t=0.6\) s. The right plot shows
    the absolute error between the fit and the bolometer inversion divided by the 
    standard deviation of the fit.}
    \label{img_post_std}
\end{figure}