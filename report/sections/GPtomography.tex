\section{Recap of Gaussian Process Tomography (GPT)}

In order to compute tomograms from given AXUV measurements the forward model 
\(\vec{P} = \mathbf{S} \cdot \vec{E}\) would have to be inverted.
This is not feasible as there are more unknown pixels than 
known measurements making the problem ill-conditioned. 
To solve this problem via Gaussian process regression we assume both our emissivity and 
our measurements to be described by a multivariate normal distribution \(\mathcal{N}\)
\begin{equation}
    \vec{P}\ \sim\ \mathcal{N}(\vec{\mu}_P, \mathbf{\Sigma}_P),\quad 
    \vec{E}\ \sim\ \mathcal{N}(\vec{\mu}_E, \mathbf{\Sigma}_E)
\end{equation}
with a respective mean \(\vec{\mu}\) and covariance \(\mathbf{\Sigma}\).
As multivariate normal distributions the probability density 
functions for \(\vec{P}\) and \(\vec{E}\) are given by
\begin{align}
    \begin{split}\label{eq_p_E}
        p(&\vec{E}) \propto
        \exp\left[-\frac{1}{2} (\vec{E}-\vec{\mu}_E)^T\mathbf{\Sigma}_E^{-1}
        (\vec{E}-\vec{\mu}_E) \right]
    \end{split}\\
    \begin{split}\label{eq_p_d_given_E}
        p(\vec{P}|&\vec{E}) \propto
        \exp\left[-\frac{1}{2} (\vec{P}-\vec{\mu}_{P|E})^T
        \mathbf{\Sigma}_P^{-1}(\vec{P}-\vec{\mu}_{P|E}) \right]
    \end{split}
\end{align}
By applying Bayes’ Theorem we can express a conditional probability 
distribution of \(\vec{E}\) for a given \(\vec{P}\)
\begin{equation}\label{eq_p_bayes}
    p(\vec{E}|\vec{P}) \propto\ p(\vec{P}|\vec{E})p(\vec{E})
\end{equation}
which as a product of two Gaussians is a Gaussian distribution itself.
\begin{equation}\label{eq_E_post}
    p(\vec{E}|\vec{P}) \propto 
    \exp\left[-\frac{1}{2} (\vec{E}-\vec{\mu}_E^{post})^T
    [\mathbf{\Sigma}_E^{post}]^{-1}(\vec{E}-\vec{\mu}_E^{post})\right]
\end{equation}
The mean of the measurements for a given emission distribution \(\vec{\mu}_{P|E}\) 
can be expressed via the forward model 
(Eq. \ref{eq_forward_model})
\begin{equation}\label{eq_d_prior}
    \vec{\mu}_{P|E} = \mathbf{S} \cdot \vec{E}
\end{equation}
By substituting the prior likelihood distribution \ref{eq_p_E}, \ref{eq_p_d_given_E} and 
Eq. \ref{eq_d_prior} into
Eq. \ref{eq_p_bayes} we can obtain the posterior mean and covariance for 
the posterior distribution given by Eq. \ref{eq_E_post} under the assumption 
that the prior mean \(\vec{\mu}_E\) is 0.
\begin{align}
    \begin{split}\label{eq_mean_post}
        &\vec{\mu}_E^{post} = \mathbf{\Sigma}_E^{post}
        \mathbf{S}^T\mathbf{\Sigma}_P^{-1}\vec{P}
    \end{split}\\
    \begin{split}\label{eq_mean_cov}
        &\mathbf{\Sigma}_E^{post} = (\mathbf{\Sigma}_E^{-1} + 
        \mathbf{S}^T\mathbf{\Sigma}_P^{-1}\mathbf{S})^{-1}
    \end{split}
\end{align}
A more thorough derivation is given in \cite{Bishop}.

In order to get the most likely emission distribution for a given AXUV 
measurement one has to construct a viable covariance/kernel matrix 
\(\mathbf{\Sigma}_E\) and use Eq. \ref{eq_mean_post}. The individual AXUV 
channels are assumed to be unrelated, because they are not physically coupled and 
local events can appear on single channels. Hence the covariance matrix for the 
measurements
\(\mathbf{\Sigma}_P\) is given by a diagonal matrix with 
\(\Sigma_{P}^{ii} = \sigma_i^2\), where \(\sigma_i^2\) is the variance of the i-th
channel given by \(\sigma_i^2=\langle\varepsilon_i^2\rangle\). A complete treatment of 
Gaussian process regression and various kernel functions can be found in 
\cite{Bishop, Rasmussen06}.