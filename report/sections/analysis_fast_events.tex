\section{Analysis of Selected Fast Events}

\subsection{Computation of calibration factors}

In the previous example a reference emissivity in form of the bolometer inversion 
was available to compute the AXUV-signals via the forward model. Since the 
goal of this work is to analyze fast events via the AXUV-diodes which cannot be examined 
with the low temporal resolution of the foil bolometers calibration factors have to be 
calculated for time points where no bolometer inversion is available for reference.

Those calibration factors are computed for each time point where a bolometer inversion 
is available (usually every millisecond of a shot). For each time point the 
AXUV-signal is computed via the forward model from the existing bolometer inversion. 
In order to get the calibration factor for the time points the computed signal is divided 
by the measured raw voltage of the AXUV-diodes. Since the time resolution of the raw 
voltage is higher (5 \(\mu\)s) the voltage is averaged over 1 ms for this.

In order to get AXUV-signals for computing inversions for arbitrary time points 
the calibration factor closest to that time point is taken and multiplied with the raw 
voltage for the time point.

\subsection{Edge Localized Modes (ELMs)}

The standard operation scenario for future Tokamaks is the high confinement 
mode (H-mode). Compared to the low confinement mode (L-mode) the H-mode is 
characterized by a transport barrier at the edge of the plasma called pedestal 
resulting in higher confinement compared to the L-mode. The pedestal features a 
high pressure gradient. If the current density resulting from the gradient 
(diamagnetic drift) reaches a threshold so called edge localized modes (ELMs) occur 
where the pedestal crashes, confinement is lowered and a significant energy/particle 
loss onto the vessel walls occurs.  
ELMs occur periodically as the pedestal recovers and crashes again and again. 
Especially for future experiments like ITER the associated high energies could 
lead to permanent damage of the device \cite{Liang2015}.
It is therefore important to find ways that reduce/mitigate ELMs. Since the time 
frame of ELMs is usually in the range of a few hundred microseconds it is impossible 
to analyze them with bolometer diagnostics and the higher temporal resolution of AXUV-diodes 
is required. 

In this work an ELM occurring at around 1.335 seconds during shot 71157 is analyzed 
with the previously described methods. The temporally closest bolometer inversion is 
taken to calculate the calibration factors. After this is done inversions with 
both GPT and minimum fisher can be computed. Figure \ref{img_elm} shows the 
results with the non-stationary kernel for three different time points together 
with the raw AXUV voltage signal for all 140 channels for the time frame of the ELM.
In order to use the non-stationary kernel in this case a two step process is used as 
no bolometer inversions are available that can be used to calculate the local length scale.
First fits are computed with the stationary kernel. Those are subsequently used to 
calculate the local length scales with which the non-stationary kernel and the inversions 
can be computed. In the inversions we can see how the ELM builds up, reaches its peak 
at around 1.3354 seconds and fades of again. Most of the energy is deposited 
into the lower left region of the vessel. 

The results for the GPT are in agreement with fits via the minimum fisher method.
For the later the problem is that a few inversion do not converge for a selected Siga 
value and trying out different values for different time points is required.

Additionally two small ELMs for shot 71158 and 71159 are analyzed. The results for 
those together with time series in GIF or MP4 format can be found in the 
Git repository for this project.


\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{71157elm.png}
    \caption{On the left fits with the non-stationary kernel for the beginning, 
    the peak and the end of the ELM are shown. To obtain those first fits with 
    the stationary kernel (\(\sigma_E = 1\) MW/m³, \(l=10\)cm) are computed. 
    From this the local length scales can be calculated. For the non-stationary kernel
    the same \(\sigma_E\) together with \(l_0 = 7\) cm and \(\delta l = 7\) cm where used.
    Additionally the magnetic flux surfaces are plotted. It should be noted that 
    those are only available in millisecond intervals. On the right the 
    raw AXUV voltage is show for all 140 channels and the duration of the ELM. Especially 
    between channel 120 and 125 it is visible that some channels are saturated. 
    The red lines correspond to the time of the fits on the left.}
    \label{img_elm}
\end{figure}


\subsection{Disruptions}

A disruption describes a complete loss of confinement/plasma control, which 
can be caused by various plasma instabilities. During a 
disruption the plasma gets in contact with the vessel walls and cools down within a few 
milliseconds. Since the energy of the plasma is dissipated onto the vessel walls in 
such a short time frame disruptions pose a great threat to the device, especially 
to upcoming devices like ITER with higher plasma energies compared to current smaller 
experiments. It is therefore important to detect the signs of disruptions and 
prevent them. Similarly to ELMs the time scale at which disruptions occur are 
too small for bolometer diagnostics, requiring the use of AXUV-diodes.

Here one disruption occurring at around 1.5 seconds during shot 70782 is analyzed. 
The procedure is the same as previously for the ELMs. It should be noted that 
the bolometer inversions stopped at around 1.497 seconds, so that last data point 
was used for calculating the calibration factor. Figure \ref{img_disruption} 
shows inversions at five selected time points and the recorded AXUV voltage 
for all channels during the disruption. For the inversions the stationary kernel 
was used since the non-stationary kernel showed abrupt jumps in the local length scales 
for consecutive time points which also lead to sudden jumps in consecutive inversions.
During the first part spatial oscillations and oscillations of the amplitude near 
the X-point can be observed. In the voltage those oscillations are also visible 
for channel 115-120. The disruption starts at 1.4994 seconds and subsequently 
a emission of tens of MW/m³ is visible close to the legs of the separatrix. 
Compared to the ELM even more channels especially between 
channel 80 to 120 show saturation during the disruption.
For the disruption the same applies regarding fits via the minimum fisher approach.
While the fits are more or less in agreement, the problem of not converging inversions 
is worse for the disruption than for the ELM

Similar to the ELMs time series in form of GIFs or MP4s can be found in the Git 
repository.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{70782disruption.png}
    \caption{Inversions for five different time points during and shortly before the 
    disruption are shown on the left. The inversions were calculated with 
    the stationary kernel (\(\sigma_E = 5\) MW/m³, \(l=10\)cm). The magnetic 
    flux surfaces were only available up to 1.499 seconds.
    On the right 
    the AXUV voltage during the disruption is plotted with red lines indicating 
    the time points of the inversions on the left. Especially between channel 80 
    and 120 a lot of channels show signs of saturation.}
    \label{img_disruption}
\end{figure}