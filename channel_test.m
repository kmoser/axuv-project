load("data/coords.mat");
load("data/geometry_axuv.mat");
load("data/vessel.mat");
load("data/transfer_matrix.mat");
mask = dlmread("data/mask.dat");

i = 15;
j = 45;

test_emission = zeros(75, 25);
test_emission(j, i) = 1e6;

channel_number = 10;

pixel_r_size = (r_coords(end) - r_coords(1))/25;
pixel_z_size = (z_coords(end) - z_coords(1))/75;

pixel_r_coords = linspace(r_coords(1), r_coords(end), 25);
pixel_z_coords = linspace(z_coords(1), z_coords(end), 75);

r_detector = geometry.xdet(channel_number);
z_detector = geometry.ydet(channel_number);
r_slit = geometry.xap(channel_number);
z_slit = geometry.yap(channel_number);
r_chord = geometry.xchord(channel_number, 1:2);
z_chord = geometry.ychord(channel_number, 1:2);
detector_size = geometry.d1(channel_number);
slit_size = geometry.b1(channel_number);
distance = geometry.distance(channel_number);

% angle different when channel starts on top/bottom or on the side
if r_chord(1) > max(r_coords)
    chord_angle = 0.5*pi - geometry.chordangle(channel_number);
    z_slit_ends = [z_slit + 0.5*slit_size*sin(chord_angle), ...
                    z_slit - 0.5*slit_size*sin(chord_angle)];
    z_detector_ends = [z_detector + 0.5*detector_size*sin(chord_angle), ...
                    z_detector - 0.5*detector_size*sin(chord_angle)];
else
    chord_angle = geometry.chordangle(channel_number);
    z_slit_ends = [z_slit - 0.5*slit_size*sin(chord_angle), ...
                    z_slit + 0.5*slit_size*sin(chord_angle)];
    z_detector_ends = [z_detector - 0.5*detector_size*sin(chord_angle), ...
                    z_detector + 0.5*detector_size*sin(chord_angle)];                
end

r_slit_ends = [r_slit - 0.5*slit_size*cos(chord_angle), ...
                    r_slit + 0.5*slit_size*cos(chord_angle)];
r_detector_ends = [r_detector - 0.5*detector_size*cos(chord_angle), ...
                    r_detector + 0.5*detector_size*cos(chord_angle)];
                
r = pixel_r_coords(i);
z = pixel_z_coords(j);

m1 = (z-z_slit_ends(1))/(r-r_slit_ends(1));
m2 = (z-z_slit_ends(2))/(r-r_slit_ends(2));
b1 = z - m1*r;
b2 = z - m2*r;

m_det = (z_detector_ends(1)-z_detector_ends(2))/(r_detector_ends(1)-r_detector_ends(2));
b_det = z_detector_ends(1) - m_det*r_detector_ends(1);

r_intercept1 = (b_det-b1)/(m1-m_det);
r_intercept2 = (b_det-b2)/(m2-m_det);
z_intercept1 = m1*r_intercept1 + b1;
z_intercept2 = m2*r_intercept2 + b2;

min_d = min(r_detector_ends);
max_d = max(r_detector_ends);

line1_inside_detector = (max_d > r_intercept1) && (r_intercept1 > min_d);
line2_inside_detector = (max_d > r_intercept2) && (r_intercept2 > min_d);

is_in_sight = ~(((r_intercept1 < min_d) && (r_intercept2 < min_d)) || ...
                ((r_intercept1 > max_d) && (r_intercept2 > max_d)));

if line1_inside_detector
    endpoint1 = [r_intercept1, z_intercept1];
    ray1 = [r-r_intercept1 z-z_intercept1];
else
    endpoint1 = [r_detector_ends(1), z_detector_ends(1)];
    ray1 = [r-r_detector_ends(1) z-z_detector_ends(1)];
end

if line2_inside_detector
    endpoint2 = [r_intercept2, z_intercept2];
    ray2 = [r-r_intercept2 z-z_intercept2];
else
    endpoint2 = [r_detector_ends(2), z_detector_ends(2)];
    ray2 = [r-r_detector_ends(2) z-z_detector_ends(2)];
end

effective_angle = acos(dot(ray1, ray2)/(norm(ray1)*norm(ray2))) * is_in_sight

effective_contribution = effective_angle/(2*pi)


channel = reshape(transfer_matrix(channel_number, :), 25, 75)';

hold on;
imagesc(r_coords, z_coords, test_emission + channel * 2e9)
axis equal; set(gca,'YDir','Normal');
colorbar;
plot(r_vessel, z_vessel, 'w');

channel_measurement = transfer_matrix(channel_number, :) * reshape(test_emission', 1875, 1)
pixel_integrated = channel_measurement*pixel_r_size*pixel_z_size

%figure;
%hold on;
%imagesc(r_coords, z_coords, channel)
%axis equal; set(gca,'YDir','Normal');
%colorbar;
%plot(r_vessel, z_vessel, 'w');