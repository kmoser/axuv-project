function scaled_matrix = downscale(matrix, scale_factor)
%DOWNSCALE scale down a given matrix by given factor

scaled_matrix = zeros(size(matrix)/scale_factor);

ii = 1;
for i = 1:size(scaled_matrix, 1)
    jj = 1;
    for j = 1:size(scaled_matrix, 2)
        subpixels = matrix(ii:ii+scale_factor-1, jj:jj+scale_factor-1);
        scaled_matrix(i,j) = mean(mean(subpixels));
        jj = jj + scale_factor;
    end
    ii = ii + scale_factor;
end
end

