function c_factor = calculate_calibration(emissivity, axuv_raw, t_emiss,... 
                                        t_axuv, transfer_matrix,        ...
                                        alternative_resolution)
%{ 
CALCULATE_CALIBRATION compute calibration factor

korbinian.moser@ipp.mpg.de
18/01/2022
                                    
PARAMETERS
----------
emissivity : float array
    bolo inversions for all time points available
axuv_raw : float array
    raw AXUV voltage/signal
t_emiss : float array
    time basis of bolo inversions
t_axuv : float array
    time basis of AXUV signal
transfer_matrix : float array
    matrix mapping channels to pixels
alternative_resolution : bool
    if true 100x40 else 75x25
                                        
RETURNS
-------
c_factor : float array
    calibration factor for AXUV calculated for each existing bolo inversion
%}

dt_emiss = (t_emiss(101)-t_emiss(1))/100;
dt_axuv = (t_axuv(101)-t_axuv(1))/100;

% half of number of inicies to average axuv signal over
to_average_over = round(0.0005/dt_axuv);

axuv_start_index = round((t_emiss(1)-t_axuv(1))/dt_axuv);
time_multiplier = dt_emiss/dt_axuv;
c_factor = zeros(size(t_emiss, 1), size(transfer_matrix, 1));

if alternative_resolution
    num_pixels = 4000;
else
    num_pixels = 1875;
end

for time_index = 1:size(t_emiss, 1)
    emiss = emissivity(:, :, time_index);
    axuv_index = round(time_index*time_multiplier + axuv_start_index);
    axuv = mean(axuv_raw(axuv_index-to_average_over:axuv_index+to_average_over, :));
    measurement = transfer_matrix * reshape(emiss, num_pixels, 1);
    c_factor(time_index, :) = measurement./axuv';
end

end

