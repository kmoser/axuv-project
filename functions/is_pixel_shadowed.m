function is_shadowed = is_pixel_shadowed(mask, pixel_coords, r_chord,   ...
                                    z_chord, r_coords_grid, z_coords_grid) 
%{ 
IS_PIXEL_SHADOWED detects if line of sight between a given pixel and a 
given diagnostic channel is obstructed for the given vessel shape. 
The solution can probably be improved a lot in terms of performance and
elegance

korbinian.moser@ipp.mpg.de
18/01/2022
                                    
PARAMETERS
----------
mask : bool array
    binary mask of vessel
pixel_coords : float array
     r,z coordinates of pixel
transfer_mat : float array
    transfer matrix mapping diagnostic channels to pixels
r_chord : float array
    start and end r-coordinate of view-chord for diagnostic channel
z_chord : float array
    same for z coordinates
r_coords_grid : float array
    r-coordinates of pixels of mask
z_coords_grid : float array
    z-coordinates of pixels of mask

RETURNS
-------
is_shadowed: bool
%}

    is_shadowed = false;
    direction = sign(z_chord(2)-z_chord(1));

    if direction == -1
        z_coords_grid = fliplr(z_coords_grid);
    else
        mask = flipud(mask);
    end

    if z_chord(1) > -0.2
        cutoff = -0.2;
    else
        cutoff = -0.4;
    end

    % TODO find faster solution
    for j = 1:size(z_coords_grid, 2)
        z = z_coords_grid(j);

        % stop loop if z is above below current pixel
        if (direction == -1) && (pixel_coords(2) > cutoff)
            break
        elseif (direction == 1) && (pixel_coords(2) < cutoff)
            break
        end

        if (z < pixel_coords(2)) && (direction == -1)
            %disp("brake1")
            break
        elseif (z > pixel_coords(2)) && (direction == 1)
            %disp("brake2")
            break
        end
        
        distance = zeros(size(r_coords_grid));
        for i = 1:size(r_coords_grid, 2)
            r = r_coords_grid(i);
            d1 = norm([r_chord(1), z_chord(1)] - [r, z]);
            d2 = norm(pixel_coords - [r, z]);
            distance(i) = d1+d2;
        end
        % get pixel in mask that is closed to line of sight between pixel 
        % and detector for current row and check if it lays within an
        % object that would obstruct vision
        [~, i_min] = min(distance);
        if mask(j, i_min) == 0
            is_shadowed = true;
            break
        end
    end
end

