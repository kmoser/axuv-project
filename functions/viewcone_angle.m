function angle = viewcone_angle(size_detector, size_slit, ...
                                distance_slit_detector)
%VIEWCONE_ANGLE calculate opening angle of view_cone for given geometry

angle = 2*atan(0.5 * (size_detector + size_slit) / ...
                        distance_slit_detector);
end

