function inversion = gti_fisher(measurements, siga, transfer_matrix, shot, time, geometry)
%{ 
GTI_FISHER wrapper function for minimum fisher tomography

korbinian.moser@ipp.mpg.de
18/01/2022
                                    
PARAMETERS
----------
measurements : float array
    measurements of AXUV, BOLO or SRX or comparable
siga : float array
    siga parameter for min fisher
transfer_matrix : float array
    transfer matrix mapping diagnostic channels to pixels
shot : uint
    number of shot
time : float
    time of measurement
z_coords : float array
    z coordinates of pixels
geometry : struct
    geomety of diagnostic

RETURNS
-------
kernel : float array
%}
       
addpath('/home/ofevrier/GIT/gti')
addpath('/home/ofevrier/GIT/radcam')

res = [25;75];

good_channels = 1:140;
diag_name = 'axuv';

xchord = geometry.xchord';
ychord = geometry.ychord';

%copy pasted measurement from forward_model_test.m
chordbrn = measurements; %size : nb channels x nb time steps

[cam_los]= [20, 40, 20, 40, 20]; %radcam_N_chn(diag_name);
cam_pos=[2 1 1 1 0];   % 2 .. upper cam, 1 .. outer cam, 0 .. lower cam

shiftangle=[];
for i=1:length(cam_pos)
    shiftangle=[shiftangle cam_pos(i)*ones(1,cam_los(i))];
end

N_chn=140;
dr = xchord(2,1:N_chn)-xchord(1,1:N_chn);
dz = ychord(2,1:N_chn)-ychord(1,1:N_chn);
pvd = atan(-dz./dr); 
pvd = mod(pvd+shiftangle*pi/2,pi)-shiftangle*pi/2;
chordgeo   = psitbxdcd(xchord(1,good_channels),ychord(1,good_channels), ...
        zeros(1,numel(good_channels)),pvd(1,good_channels), ...
        zeros(1,numel(good_channels)),501,[]);


%% Set resolution and grid 
rlim   = [0.624, 1.135];
zlim   = [-0.75, 0.75];
method = 'MinFishReg';
grid_shape = 'RectangularPixels';

%% Prepare GTI
gti = gti_get_defaults( shot, 1, time, 'AXUV', grid_shape, method);
gti{1} = gti_check_params(gti{1}.shot,1,gti{1});
gti{1}.diag.name = 'AXUV_RADCAM';

% IF YOU WANT TO USE YOUR OWN T MATRIX

if ~isempty(transfer_matrix)
    gti{1}.diag.Tmatrix = transfer_matrix;
end

gti{1}.disc.s.num = res;
gti{1}.disc.s.lim(2,:)=zlim;
gti{1}.disc.s.lim(1,:)=rlim;

gti{1}.diag.time = time;
gti{1}.disc.s.nt = numel(time);
gti{1}.diag.energy.nchan=1;
gti{1}.diag.energy.channel=0;

gti{1}.method.useWeights=1;
gti{1}.method.invparam.siga=siga;
gti{1}.method.invparam.add_zero_chord=0;
gti{1}.method.invparam.nfishmax = 30;
gti{1}.method.invparam.nalfamax = 30;
gti{1}.method.invparam.parallel = 0;
gti{1}.method.invparam.every_timepoint = 0;
%if(filtercutoff>100)
%gti{1}.method.invparam.every_timepoint = 1;
%end

gti{1}.diag.chordbrn = chordbrn;
gti{1}.diag.chordgeo = chordgeo;
gti{1}.diag.chn      = good_channels;
gti{1}.disc          = gti_get_disc( gti{1}.shot, version, gti{1}.diag, gti{1}.disc);
% Run the inversion 
gti{1}.res    = gti_do_inversion(gti{1}.shot,gti{1}.version,gti{1}.diag,gti{1}.disc,gti{1}.method);
% Post-process the inversion 
gti{1}        = gti_post_processing(gti{1}.shot,gti{1}.version,gti{1});
%Output structure

inversion = reshape(gti{1,1}.res.inverted, 25, 75)';

end

