function [diag,sxr] = load_diodes(varargin)
    % Function [diag,sxr] = load_diodes(varargin)
    % Load channel(s) data for radcam diodes (axuv and sxr)
    % luke.simons@epfl.ch
    % 14/10/2021
    % INPUT:
    %   Req: shot - uint, the shot to load
    %   Par: channels - uint array, the channels to load
    %       default is all channels for axuv diagnostic
    %   Par: diag_name - string, name of diagnostic, "axuv", "sxr" or "all"
    %       default = "all"
    % OUTPUT:
    %   Def: diag - struct, Array of times and signals for channels
    %   Opt: sxr - struct, Array of times and signals for channels
    

    %% PARSE INPUT
    parser = inputParser;
    check_shot_Fcn = @(x) isnumeric(cast(x,'uint32')) && x>0;
    channels=1:140; diag_name="all"; sxr=struct;
    addRequired(parser,'shot',check_shot_Fcn);
    addParameter(parser,'channels',channels);
    addParameter(parser,'diag_name',diag_name,@isstring);
    parse(parser,varargin{:});
    shot=parser.Results.shot;
    channels=parser.Results.channels;
    diag_name=parser.Results.diag_name;
    

    %% Channel mapping
    mapping_a_vt=zeros(1,20);
    mapping_a_ul=zeros(1,40);
    mapping_a_ml_140=zeros(1,20);
    mapping_a_ml_120=zeros(1,40);
    mapping_a_vb=zeros(1,20); 
    mapping_a_ll=zeros(1,40);
    
    mapping_s_vt=zeros(1,20);
    mapping_s_ul=zeros(1,20);
    mapping_s_ml_100=zeros(1,20);
    mapping_s_ml_60=zeros(1,20);
    mapping_s_vb=zeros(1,20);

    % Mapping verified by Luke Simons on 17/11/21 against channel mapping
    % provided by Olivier Villinger, see /home/lsimons/public/DIODES/
    % and against physical inspection of cable connections (17/11/21)
    % These mappings have not changed since first installation...
    mapping_a_vt(1:2:19)=[53:62]; mapping_a_vt(2:2:20)=[10:-1:1];
    mapping_a_ll(1:2:39)=[75:84,21:30]; mapping_a_ll(2:2:40)=[94:-1:85,74:-1:65];
    mapping_a_vb(1:2:19)=[11:20]; mapping_a_vb(2:2:20)=[30:-1:21];

    mapping_s_vt(1:2:19)=[43:52]; mapping_s_vt(2:2:20)=[42:-1:33];
    mapping_s_vb(1:2:19)=[53:62]; mapping_s_vb(2:2:20)=[10:-1:1];

    N_chn_sxr=0;
    N_chn_axuv=0;
    old_mapping=false;
    if shot <= 70128
        old_mapping=true;
        N_chn_sxr=60;
        N_chn_axuv=120;
        
        % AXUV is on Board 11
        % a_ml=b11([33:42 75:84 43:52 85:94],:); a_ml=[a_ml(ordering,:)./1; a_ml(ordering+20,:)];
        mapping_a_ml_120(1:2:19)=[85:94];  mapping_a_ml_120(2:2:20)=[52:-1:43];
        mapping_a_ml_120(21:2:39)=[75:84]; mapping_a_ml_120(22:2:40)=[42:-1:33];
        
        % SXR is on Board 13
        mapping_s_ml_60(1:2:19)=[1:10]; mapping_s_ml_60(2:2:20)=[20:-1:11];

        % Mid-lateral camera was not installed.
        % Top-lateral camera installed in mid lateral position.
        mapping_a=[mapping_a_vt,mapping_a_ml_120,mapping_a_ll,mapping_a_vb];
        mapping_s=[mapping_s_vt,mapping_s_ml_60,mapping_s_vb];
        
        %Board Mapping
        %board_map_a=[repmat(12,1,20),repmat(11,1,40),repmat(12,1,20),repmat([13,12],1,10),repmat(11,1,20)]';
        %board_map_s=[repmat(12,1,20),repmat(13,1,20),repmat(11,1,20)]';
        % Factor mapping
        %factor_map_a=[repmat(2,1,20),repmat(1,1,40),repmat(2,1,20),repmat([1,2],1,10),repmat(1,1,20)]';
        %factor_map_s=[repmat(2,1,20),repmat(1,1,20),repmat(1,1,20)]';

    elseif shot > 70128
        old_mapping=false;
        N_chn_sxr=100;
        N_chn_axuv=140;
        % Following line is old mapping that matches radcam_diodes_dld
        %mapping_a_ul(1:2:39)=[33:42,11:20]; mapping_a_ul(2:2:40)=[20:-1:11,74:-1:65];
        % Following line is the mapping I figured out from inversions
        mapping_a_ul(1:2:39)=[11:20,11:20]; mapping_a_ul(2:2:40)=[74:-1:65,42:-1:33];
        mapping_a_ml_140(1:2:19)=[1:10];  mapping_a_ml_140(2:2:20)=[52:-1:43];
        
        % Following line is old mapping for unplugged cable discovered on 
        % the 16/11/2021 (Possibly not connected since 70128?...)
        %mapping_s_ul(1:2:19)=[21:30]; mapping_s_ul(2:2:20)=[62:-1:53];
        % Following line *should* work since new connections 17/11/21
        % NEEDS TESTING
        mapping_s_ul(1:2:19)=[85:94]; mapping_s_ul(2:2:20)=[30:-1:21];
        %mapping_s_ml_100=zeros(1,40); mapping_s_ml_100(1:2:39)=[85:94,43:52]; mapping_s_ml_100(2:2:40)=[42:-1:33,74:-1:65];
        % Following line is old mapping that matches radcam_diodes_dld
        %mapping_s_ml_100(1:2:39)=[33:52]; mapping_s_ml_100(2:2:20)=[84:-1:75]; mapping_s_ml_100(22:2:40)=[94:-1:85];
        % Following 2 lines are the mapping I figured out from inversions
        % On the 05/11/21
        mapping_s_ml_100(1:2:19)=[85:94]; mapping_s_ml_100(21:2:39)=[75:84]; 
        mapping_s_ml_100(2:2:20)=[52:-1:43]; mapping_s_ml_100(22:2:40)=[42:-1:33]; 

        mapping_a=[mapping_a_vt,mapping_a_ul,mapping_a_ml_140,mapping_a_ll,mapping_a_vb];
        mapping_s=[mapping_s_vt,mapping_s_ul,mapping_s_ml_100,mapping_s_vb];
        
        %Board Mapping
        %board_map_a=[repmat(12,1,20),repmat(13,1,20),repmat([12,11],1,10),repmat(13,1,20),repmat(12,1,20),repmat([13,12],1,10),repmat(11,1,20)]';
        %board_map_s=[repmat(12,1,20),repmat([12,13],1,10),repmat(11,1,40),repmat(11,1,20)]';
        % Factor mapping
        %factor_map_a=[repmat(2,1,20),repmat(1,1,20),repmat([2,1],1,10),repmat(1,1,20),repmat(2,1,20),repmat([1,2],1,10),repmat(1,1,20)]';
        %factor_map_s=[repmat(2,1,20),repmat([2,1],1,10),repmat(2,1,40),repmat(1,1,20)]';

    end
    
    %% Checks
    if strcmp(diag_name,"sxr")
        if ( length(channels)>N_chn_sxr )
            channels=1:N_chn_sxr;
        end
        if( max(channels)>N_chn_sxr )
            error('max channel index %i > %i for sxr',max(channels),N_chn_sxr);
        end
    elseif strcmp(diag_name,"axuv")
        if ( length(channels)>N_chn_axuv )
            channels=1:N_chn_axuv;
        end
        if( max(channels)>N_chn_axuv )
            error('max channel index %i > %i for sxr',max(channels),N_chn_axuv);
        end
    elseif strcmp(diag_name,"all")
        diag=load_diodes(shot,'diag_name',"axuv",'channels',[1:N_chn_axuv]);
        sxr=load_diodes(shot,'diag_name',"sxr",'channels',[1:N_chn_sxr]);
        return;
    else
        error('Input diag_name: "%s" not handled',diag_name);
    end
    
    
    %% Channel mapping procedure adapted from:
    % /home/usheikh/div_upgrade/clean2021/radcam_diodes_dld.m
    %% Connect to mds and check raw data data is available

    mdsopen(shot);
    t=mdsvalue('dim_of(\atlas::dt196_axuv_003:CHANNEL_001)');
    fprintf(1,'Downloading %s Data for shot %i\n',diag_name,shot);
    try
    	mdsopen('axuv',shot);
    catch
        error('Failed to open MDS server');
    end

    signals=nan(length(channels),length(t));
    board=0;
    for i = 1:length(channels)
        p=channels(i);
        factor=1; % Account for factor of 2 in board 12
        if strcmp(diag_name,"sxr") % If it is sxr
            if old_mapping
                if p < 21 % vt
                    board=12; factor=2;
                elseif p < 41 % Upper lateral camera installed in mid lateral port
                    board=13;
                else % vb
                    board=11;
                end
            else
                if p < 21 % vt
                    board=12; factor=2;
                elseif p < 41 % ul
                    if isodd(p)
                        board=13;
                    else
                        board=12; factor=2;
                    end
                elseif p < 81 % ml
                    % Added 2.1 factor on 03/11/2021 after discussion with Umar
                    % Detailed investigation of inversion for shot 72186 show
                    % something wrong with the gains since the SXR signal sits
                    % at one position for an extended period of time.

                    % Umar found a note which indicated a factor of 2.1
                    % for now, a factor of 2 is used instead
                    board=11; factor=2; 
                else % vb
                    board=11;
                end
            end
            ch=mapping_s(p);
        elseif strcmp(diag_name,"axuv") % If it is axuv
            if old_mapping
                if p < 21 % vt
                    board=12; factor=2;
                elseif p < 61 % Upper lateral camera installed in mid lateral port
                    board=11;
                elseif p < 81 % ll 1
                    board=12; factor=2;
                elseif p < 101 % ll 2
                    if isodd(p)
                        board=13;
                    else
                        board=12; factor=2;
                    end
                else % vb
                    board=11;
                end
            else
                if p < 21 % vt
                    board=12; factor=2;
                elseif p < 41 % ul 1
                    if isodd(p)
                        board=12; factor=2; 
                    else
                        board=11; 
                    end
                elseif p < 61 % ul 2
                    board=13;
                elseif p < 81 % ml
                    board=13;
                elseif p < 101 % ll 1
                    board=12; factor=2;
                elseif p < 121 % ll 2
                    if isodd(p)
                        board=13;
                    else
                        board=12; factor=2;
                    end
                else % vb
                    board=11;
                end
            end
            ch=mapping_a(p);
        else
            error('Input diag_name: "%s" not handled',diag_name);
        end
        
        % Compose mds string from board and channel
        n=['.BOARD_' num2str(board,'%03.0f') ':CHANNEL_' num2str(ch,'%03.0f')];
        try % Try loading the data
            raw_data=mdsvalue(n);
            if( ~ischar(raw_data) ) 
                signals(i,:)=raw_data(1:length(t))./(2^15.*factor);
                signals(i,:)=signals(i,:)-mean(signals(i,end-1000:end)); % Remove offset
            end
        catch % If it fails, close
            fprintf(1,'Warning, failed to load data for index::channel %i:%i',i,ch)
        end
    end
    
    mdsclose();
    
    % Return the data
    diag.time=t;
    diag.data=signals';
    
end

