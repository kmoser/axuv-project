function params = optimize_parameters(measurements, kernel_type, initial_params, alternative_resolution, emissivity)
%{ 
OPTIMIZE_PARAMETERS find best parameters by maximizing marginal log
likelihood

korbinian.moser@ipp.mpg.de
18/01/2022
                                    
PARAMETERS
----------
params : float array
    parameters of kernel
kernel_type : string
    "statinonary" or "non_stationary"
initial_params : float array
    initial parameters for optimization
alternative_resolution : bool
    if true resolution is 100x40 else 75x25
emissivity : float array
    only used for non-stationary kernel set to [] otherwise

RETURNS
-------
params : float array
    optimized parameters
%}
    if alternative_resolution
        T = load("data/transfer_matrix100x40.mat");
        coords = load("data/coords100x40.mat");
    else    
        T = load("data/transfer_matrix.mat");
        coords = load("data/coords.mat");
    end
    
    T = T.transfer_matrix;
    
    % neglect channels with negative or no measurement
    for channel = 1:size(T, 1)
        if measurements(channel) <= 0
            T(channel, :) = zeros(1, size(T, 2));
        end
    end
    
    % define negative marginal log likelihood depending on parameters
    function value = neg_log_likelihood(params)
       
        if kernel_type == "stationary"
            kernel = get_kernel(params, coords.r_coords, coords.z_coords);
        elseif kernel_type == "non_stationary"
            kernel = get_non_stationary_kernel(params, emissivity, ...
                                    coords.r_coords, coords.z_coords);
        else
            error('kernel function does not exist')
        end
        
        variance = (0.05 * measurements + 1).^2;

        sigma_d = diag(variance);
        
        intermediate = T * kernel * T' + sigma_d;
        L = chol(intermediate);
        half_log_det = sum(log(diag(L)));
        
        value = half_log_det + ...
            0.5*(measurements' * inv(intermediate) * measurements);
    end

    options = optimset('Display','iter', "TolFun", 0.01, "TolX", 0.001);
    params = fminsearch(@neg_log_likelihood, initial_params, options);
end

