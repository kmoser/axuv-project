function kernel = get_kernel(params, r_coords, z_coords)
%{ 
GET_KERNEL returns stationary cartesian kernel with given parameters

korbinian.moser@ipp.mpg.de
18/01/2022
                                    
PARAMETERS
----------
params : float array
    parameters of kernel (sigma and length scale)
r_coords : float array
    r coordinates of pixels
z_coords : float array
    z coordinates of pixels

RETURNS
-------
kernel : float array
%}

    kernel = zeros(size(r_coords, 1)*size(z_coords, 1));

    for r1 = 1:size(r_coords)
        for z1 = 1:size(z_coords)

            index1 = (z1-1)*length(r_coords) + r1;

            for r2 = 1:size(r_coords)
                for z2 = 1:size(z_coords)

                    index2 = (z2-1)*length(r_coords) + r2;
                    sqdist = (r_coords(r1) - r_coords(r2))^2 + ...
                            (z_coords(z1) - z_coords(z2))^2;
                    kernel(index1, index2) = params(1)^2 * ...
                            exp(-sqdist/(2*params(2)^2));
                end
            end  
        end
    end
end

