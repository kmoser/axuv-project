function [post_mean, post_std] = perform_inversion(measurements, params,...
                                    transfer_mat, r_coords, z_coords,   ...
                                    kernel_inv, alternative_resolution)
%{ 
PERFORM_INVERSION compute tomogram for given measurements, 
kernel and parameters via GPT

korbinian.moser@ipp.mpg.de
18/01/2022
                                    
PARAMETERS
----------
measurements : float array
    measurements of AXUV, BOLO or SRX or comparable
params : float array
    parameters of kernel (sigma, l, ...)
transfer_mat : float array
    transfer matrix mapping diagnostic channels to pixels
r_coords : float array
    r coordinates of pixels
z_coords : float array
    z coordinates of pixels
kernel_inv : float array
    inverted kernel matrix, only needed when a lot data points have 
    have to be calculated to prevent inversion each time and speed things
    up. Set to [] if not needed.
alternative_resolution : bool
    false: 75x25 resolution, true: 100x40 resolution; make sure to 
    also use the right r/z coords and transfer matrix for choosen
    resolution

RETURNS
-------
post_mean : float array
    mean of posteror distribution, i.e. the computed tomogram
post_std : float array
    std of fitted emission (sqrt of diagonal of posteror covariance)
%}

    %TODO implement prameter that let you select the kernel function
    % right now always the stationary one is used. Use the non-stationary 
    % one via the kernel_inv
    if isempty(kernel_inv)
        kernel = get_kernel(params, r_coords, z_coords);
        % add small constant to diagonal to reduce condition number
        kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
    end

    variance = (0.05 * measurements + 1).^2;
    
    % don't use channels with negative measurement
    for channel = 1:size(transfer_mat, 1)
        if measurements(channel) <= 0
            transfer_mat(channel, :) = zeros(1, size(transfer_mat, 2));
        end
    end

    sigma_d_inv = diag(1./variance);

    sigma_post_inv = transfer_mat' * sigma_d_inv * transfer_mat + kernel_inv;
    %cond(sigma_post_inv)
    post_mean = sigma_post_inv \ transfer_mat' * sigma_d_inv * measurements;

    post_std = sqrt(diag(inv(sigma_post_inv)));

    if alternative_resolution
        post_mean = reshape(post_mean, 40, 100)';
    else
        post_mean = reshape(post_mean, 25, 75)';
    end
end

