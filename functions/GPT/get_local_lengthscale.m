function local_l = get_local_lengthscale( params, r_max, z_max, r_coords, z_coords )
%{ 
GET_KERNEL returns local length scales for non-stationary cartesian kernel

korbinian.moser@ipp.mpg.de
18/01/2022
                                    
PARAMETERS
----------
params : float array
    parameters (l_0, delta-l)
r_max : float
    r-coordinate of max emissivity
z_max : float
    z-coordinate of max emissivity
r_coords : float array
    r coordinates of pixels
z_coords : float array
    z coordinates of pixels

RETURNS
-------
local_l : float array
    length scale for each pixel
%}

local_l = zeros(size(z_coords, 1),size(r_coords, 1));

for i = 1:size(local_l, 1)
   z = z_coords(i);
   for j = 1:size(local_l, 2)
       r = r_coords(j);
       dist = sqrt((r_max - r)^2 + (z_max - z)^2);

       local_l(i,j) = params(1) + params(2)/2 * (1 + tanh((dist-0.22)/0.12));
   end
end

end

