function kernel = get_non_stationary_kernel(params, emissivity, r_coords, z_coords)
%{ 
GET_KERNEL returns non-stationary cartesian kernel with given parameters

korbinian.moser@ipp.mpg.de
18/01/2022
                                    
PARAMETERS
----------
params : float array
    parameters of kernel (sigma and l_0, delta-l)
emissivity : float array
    emissivity either from bolo inversion or GPT with stationary kernel
    to obtain maximum from and calculate local length scales
r_coords : float array
    r coordinates of pixels
z_coords : float array
    z coordinates of pixels

RETURNS
-------
kernel : float array
%}

[zi, ri] = find(emissivity == max(max(emissivity)));

r_max = r_coords(ri);
z_max = z_coords(zi);

local_lengthscale = get_local_lengthscale(params(2:3), r_max, z_max, r_coords, z_coords );

kernel = zeros(size(r_coords, 1)*size(z_coords, 1));

for r1 = 1:size(r_coords)
    for z1 = 1:size(z_coords)
        
        local_l1 = local_lengthscale(z1, r1);
        
        index1 = (z1-1)*length(r_coords) + r1;

        for r2 = 1:size(r_coords)
            for z2 = 1:size(z_coords)
                
                local_l2 = local_lengthscale(z2, r2);
                
                index2 = (z2-1)*length(r_coords) + r2;
                sqdist = (r_coords(r1) - r_coords(r2))^2 + ...
                        (z_coords(z1) - z_coords(z2))^2;
                kernel(index1, index2) = params(1)^2 * ...
                        exp(-sqdist/(local_l1^2 + local_l2^2));
            end
        end  
    end
end
end

