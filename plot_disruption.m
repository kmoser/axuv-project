addpath('functions/crpptbx-9.2.0/');
addpath('functions/', 'functions/GPT/');
load('data/coords.mat');
load('data/vessel.mat');
load('data/shot70782emissivity.mat');
load('data/shot70782axuvraw.mat');
load('data/transfer_matrix.mat');
mask = dlmread('data/mask.dat');

timebasis_axuv = shot70782axuvraw.time;
%axuv_raw_data = movmean(shot70782axuvraw.data, [stepsize, 0]);
axuv_raw_data = shot70782axuvraw.data;
emissivity = shot70782emissivity.data;
timebasis_emiss = shot70782emissivity.dim{3};

cal_data = calculate_calibration(   shot70782emissivity.data,   ...
                                   shot70782axuvraw.data,      ...
                                   timebasis_emiss, ...
                                   timebasis_axuv,      ...
                                   transfer_matrix,    ...
                                   false);

cal_data_at_t = cal_data(end, :);
axuv_start_index = round((1.497 + 0.04)/0.000005);
axuv_end_index = round((1.502 + 0.04)/0.000005);
time_interval = axuv_start_index:axuv_end_index;

axuv_channel14_t_interval = axuv_raw_data(axuv_start_index:axuv_end_index, 14)*cal_data_at_t(14);

kernel = get_kernel([5000000 0.1], r_coords, z_coords);
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));

time_index = time_interval(204);
measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
fit1 = perform_inversion(measurement', [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
fit1(fit1<0) = 0;

time_index = time_interval(219);
measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
fit2 = perform_inversion(measurement', [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
fit2(fit2<0) = 0;

time_index = time_interval(560);
measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
fit3 = perform_inversion(measurement', [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
fit3(fit3<0) = 0;

time_index = time_interval(620);
measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
fit4 = perform_inversion(measurement', [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
fit4(fit4<0) = 0;

time_index = time_interval(750);
measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
fit5 = perform_inversion(measurement', [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
fit5(fit5<0) = 0;

color = [0.6 0.6 0.6]; shot = 70782;

max_emiss = max(max(max([fit1, fit2, fit3, fit4, fit5])));

figure;

subplot('position', [0.05 0.1 0.11 0.75])
hold on;
time = timebasis_axuv(time_interval(204));
clims = [0 max_emiss];
imagesc(r_coords, z_coords, reshape(fit1, 75, 25) .* flipud(mask), clims);
tcv_polview('vtTPp',shot, time,'Color_p',color,'Color_b',color,'Color_P',color);
axis equal; set(    gca,'YDir','Normal');
plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
title(sprintf('t=%.5f s', time));
ylabel("z in m");
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot('position', [0.16 0.1 0.11 0.75])
hold on;
time = timebasis_axuv(time_interval(219));
clims = [0 max_emiss];
imagesc(r_coords, z_coords, reshape(fit2, 75, 25) .* flipud(mask), clims);
tcv_polview('vtTPp',shot, time,'Color_p',color,'Color_b',color,'Color_P',color);
axis equal; set(    gca,'YDir','Normal');
plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
title(sprintf('t=%.5f s', time));
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])
set(gca,'ytick',[])
set(gca,'yticklabel',[])

subplot('position', [0.27 0.1 0.11 0.75])
hold on;
time = timebasis_axuv(time_interval(560));
clims = [0 max_emiss];
imagesc(r_coords, z_coords, reshape(fit3, 75, 25) .* flipud(mask), clims);
tcv_polview('vtTPp',shot, time,'Color_p',color,'Color_b',color,'Color_P',color);
axis equal; set(    gca,'YDir','Normal');
plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
title(sprintf('t=%.5f s', time));
xlabel("R in m");
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])
set(gca,'ytick',[])
set(gca,'yticklabel',[])

subplot('position', [0.38 0.1 0.11 0.75])
hold on;
time = timebasis_axuv(time_interval(620));
clims = [0 max_emiss];
imagesc(r_coords, z_coords, reshape(fit4, 75, 25) .* flipud(mask), clims);
tcv_polview('vtTPp', shot, time,'Color_p',color,'Color_b',color,'Color_P',color);
axis equal; set(    gca,'YDir','Normal');
plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
title(sprintf('t=%.5f s', time));
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])
set(gca,'ytick',[])
set(gca,'yticklabel',[])

subplot('position', [0.49 0.1 0.17 0.75])
hold on;
time = timebasis_axuv(time_interval(750));
clims = [0 max_emiss];
imagesc(r_coords, z_coords, reshape(fit5, 75, 25) .* flipud(mask), clims);
tcv_polview('vtTPp',shot, time,'Color_p',color,'Color_b',color,'Color_P',color);
axis equal; set(    gca,'YDir','Normal');
plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
title(sprintf('t=%.5f s', time));
c = colorbar;
c.Label.String = ("W/m³");
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])
set(gca,'ytick',[])
set(gca,'yticklabel',[])

subplot('position', [0.72 0.1 0.25 0.75])
hold on;
imagesc(1:140, timebasis_axuv(time_interval, :), axuv_raw_data(time_interval, :))
plot(1:140, repelem(timebasis_axuv(time_interval(204)), 140), 'color', 'r')
plot(1:140, repelem(timebasis_axuv(time_interval(219)), 140), 'color', 'r')
plot(1:140, repelem(timebasis_axuv(time_interval(560)), 140), 'color', 'r')
plot(1:140, repelem(timebasis_axuv(time_interval(620)), 140), 'color', 'r')
plot(1:140, repelem(timebasis_axuv(time_interval(750)), 140), 'color', 'r')
title(sprintf('AXUV Raw Signal', time));
xlabel("channel");
ylabel("time in s");
xlim([1 140])
ylim([timebasis_axuv(axuv_start_index) timebasis_axuv(axuv_end_index)])
c = colorbar;
c.Label.String = ("voltage in V");

suptitle('Shot 70782')