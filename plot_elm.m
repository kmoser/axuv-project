addpath('functions/crpptbx-9.2.0/');
addpath('functions/', 'functions/GPT/');
load('data/vessel.mat');
load('data/coords.mat');
load('data/geometry_axuv.mat');
load('data/shot71157emissivity.mat');
load('data/shot71157axuvraw.mat');
T = load('data/transfer_matrix.mat');
T100x40 = load('data/transfer_matrix100x40.mat');
mask = dlmread('data/mask.dat');

%axuv_raw_data = movmean(shot70782axuvraw.data, [stepsize, 0]);
time_basis_emiss = shot71157emissivity.dim{3};
time_basis_axuv = shot71157axuvraw.time;
axuv_raw_data = shot71157axuvraw.data;


cal_data = calculate_calibration(   shot71157emissivity.data,   ...
                                    axuv_raw_data,      ...
                                    time_basis_emiss, ...
                                    time_basis_axuv,      ...
                                    T100x40.transfer_matrix,        ...
                                    true);
                             
time_index_emiss = 536;
cal_data_at_t = cal_data(time_index_emiss, :);

indicies_axuv = 275020:275180;

kernel = get_kernel([1000000 0.1], r_coords, z_coords);
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));

time_index = indicies_axuv(20);
measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
fit1 = perform_inversion(measurement', [], T.transfer_matrix, r_coords, z_coords, kernel_inv, false);
kernel = get_non_stationary_kernel([1000000, 0.07, 0.07], reshape(fit1, 75, 25), r_coords, z_coords);
kernel = kernel + 0.001*diag(diag(kernel));
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
fit1 = perform_inversion(measurement', [], T.transfer_matrix, r_coords, z_coords, kernel_inv, false);
fit1(fit1<0) = 0;

time_index = indicies_axuv(60);
measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
fit2 = perform_inversion(measurement', [], T.transfer_matrix, r_coords, z_coords, kernel_inv, false);
kernel = get_non_stationary_kernel([1000000, 0.07, 0.07], reshape(fit2, 75, 25), r_coords, z_coords);
kernel = kernel + 0.001*diag(diag(kernel));
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
fit2 = perform_inversion(measurement', [], T.transfer_matrix, r_coords, z_coords, kernel_inv, false);
fit2(fit2<0) = 0;

time_index = indicies_axuv(110);
measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
fit3 = perform_inversion(measurement', [], T.transfer_matrix, r_coords, z_coords, kernel_inv, false);
kernel = get_non_stationary_kernel([1000000, 0.07, 0.07], reshape(fit3, 75, 25), r_coords, z_coords);
kernel = kernel + 0.001*diag(diag(kernel));
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
fit3 = perform_inversion(measurement', [], T.transfer_matrix, r_coords, z_coords, kernel_inv, false);
fit3(fit3<0) = 0;


color = [0.6 0.6 0.6]; shot = 71157;

subplot('position', [0.05 0.1 0.15 0.75])
hold on;
time = time_basis_axuv(indicies_axuv(20));
clims = [0 max(max(max(fit2)))];
imagesc(r_coords, z_coords, reshape(fit1, 75, 25) .* flipud(mask), clims);
tcv_polview('vtTPp',shot,time,'Color_p',color,'Color_b',color,'Color_P',color);
axis equal; set(    gca,'YDir','Normal');
title(sprintf('t=%.5f s', time));
ylabel("z in m");
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot('position', [0.20 0.1 0.15 0.75])
hold on;
time = time_basis_axuv(indicies_axuv(60));
clims = [0 max(max(max(fit2)))];
imagesc(r_coords, z_coords, reshape(fit2, 75, 25) .* flipud(mask), clims);
tcv_polview('vtTPp',shot,time,'Color_p',color,'Color_b',color,'Color_P',color);
axis equal; set(    gca,'YDir','Normal');
title(sprintf('t=%.5f s', time));
xlabel("R in m");
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])
set(gca,'ytick',[])
set(gca,'yticklabel',[])


subplot('position', [0.35 0.1 0.2 0.75])
hold on;
time = time_basis_axuv(indicies_axuv(110));
clims = [0 max(max(max(fit2)))];
imagesc(r_coords, z_coords, reshape(fit3, 75, 25) .* flipud(mask), clims);
tcv_polview('vtTPp',shot, time,'Color_p',color,'Color_b',color,'Color_P',color);
axis equal; set(    gca,'YDir','Normal');
title(sprintf('t=%.5f s', time));
c = colorbar;
c.Label.String = ("W/m³");
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])
set(gca,'ytick',[])
set(gca,'yticklabel',[])

subplot('position', [0.65 0.1 0.3 0.75])
hold on;
imagesc(1:140, time_basis_axuv(indicies_axuv, :), axuv_raw_data(indicies_axuv, :))
plot(1:140, repelem(time_basis_axuv(indicies_axuv(20)), 140), 'color', 'r')
plot(1:140, repelem(time_basis_axuv(indicies_axuv(60)), 140), 'color', 'r')
plot(1:140, repelem(time_basis_axuv(indicies_axuv(110)), 140), 'color', 'r')
title(sprintf('AXUV Raw Signal', time));
xlabel("channel");
ylabel("time in s");
xlim([1 140])
ylim([time_basis_axuv(indicies_axuv(1)) time_basis_axuv(indicies_axuv(end))])
c = colorbar;
c.Label.String = ("voltage in V");

suptitle('Shot 71157')