addpath('functions/crpptbx-9.2.0/');
addpath('functions/', 'functions/GPT/');
load('data/vessel.mat');
load('data/coords.mat');
load('data/geometry_axuv.mat');
load('data/shot71157emissivity.mat');
load('data/shot71157axuvraw.mat');
T = load('data/transfer_matrix.mat');
T100x40 = load('data/transfer_matrix100x40.mat');
mask = dlmread('data/mask.dat');

%r_coords = shot71157emissivity.dim{1};
%z_coords = shot71157emissivity.dim{2};
time_basis_emission = shot71157emissivity.dim{3};
time_basis_axuv = shot71157axuvraw.time;
axuv_raw_data = shot71157axuvraw.data;

cal_data = calculate_calibration(   shot71157emissivity.data,   ...
                                    axuv_raw_data,      ...
                                    time_basis_emission, ...
                                    time_basis_axuv,      ...
                                    T100x40.transfer_matrix,        ...
                                    true);
                                
cal_data_at_t = cal_data(536, :);

%indicies_axuv = 275060:275100;
indicies_axuv = 275020:275180;

%axuv_raw_data(:, [114 116 118 120]) = zeros(515481, 4);

% figure;
% hold on;
% emiss = shot71157emissivity.data(:, :, 536)';
% imagesc(r_coords, z_coords, emiss)
% colorbar;
% plot(r_vessel, z_vessel)
% axis equal; set(    gca,'YDir','Normal');
% 
% measurement = (axuv_raw_data(indicies_axuv(50), :) .* cal_data_at_t)';
% figure;
% plot(measurement)
% figure;
% plot(transfer_matrix * reshape(emiss', 4000, 1))
% 
% params = optimize_parameters(measurement, [100000, 0.08], true);
% 
% fit = perform_inversion(measurement, params, transfer_matrix, r_coords, z_coords, [], true);
% fit(fit<0) = 0;
% figure;
% hold on;
% imagesc(r_coords, z_coords, fit)
% plot(r_vessel, z_vessel)
% colorbar;
% axis equal; set(    gca,'YDir','Normal');


kernel = get_kernel([1000000 0.1], r_coords, z_coords);
kernel = kernel + 0.001*diag(diag(kernel));
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));

inversions = zeros(size(indicies_axuv,2), 75, 25);
inversions_var_l = zeros(size(indicies_axuv,2), 75, 25);
measurements = zeros(size(indicies_axuv,2), 140);

for i = 1:size(indicies_axuv, 2)
    time_index = indicies_axuv(i)
    time = time_basis_axuv(time_index)
    measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
    measurements(i, :) = measurement;
    e = gti_fisher(measurement', 0.05, T.transfer_matrix, 71157, time, geometry);
    %e = perform_inversion(measurement', [], T.transfer_matrix, r_coords, z_coords, kernel_inv, false);
    e(e<0) = 0;
    inversions(i, :, :) = e;
end

% for i = 1:size(indicies_axuv, 2)
%     time_index = indicies_axuv(i)
%     time = time_basis_axuv(time_index)
%     measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
%     kernel = get_non_stationary_kernel([1000000, 0.07, 0.07], reshape(inversions(i, :, :), 75, 25), r_coords, z_coords);
%     kernel = kernel + 0.001*diag(diag(kernel));
%     kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
%     e = perform_inversion(measurement', [], T.transfer_matrix, r_coords, z_coords, kernel_inv, false);
%     e(e<0) = 0;
%     inversions_var_l(i, :, :) = e;
% end

for i = 1:size(indicies_axuv, 2)
    time_index = indicies_axuv(i)
    time = time_basis_axuv(time_index)
    figure('Position',[10 10 800 600]);
    subplot(121)
    hold on;
    clims = [0 max(max(max(inversions)))];
    imagesc(r_coords, z_coords, reshape(inversions(i, :, :), 75, 25) .* flipud(mask), clims);
    axis equal; set(    gca,'YDir','Normal');
    plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
    xlabel('r in m'); ylabel('z in m');
    title(sprintf('71157 AXUV t=%.4f', time));
    colorbar;
    subplot(122)
    hold on;
    imagesc(1:140, time_basis_axuv(indicies_axuv, :), axuv_raw_data(indicies_axuv, :))
    plot(1:140, repelem(time, 140), 'color', 'r')
    filename = sprintf('results/shot71157elm/t=1335fisher/emiss%7d.png', round(time*1000000));
    saveas(gcf, filename);
    close all;
end
%        
% hold on;
% imagesc(1:140, time_basis_axuv(indicies_axuv, :), axuv_raw_data(indicies_axuv, :)); set(    gca,'YDir','Normal');
% plot(1:140, repelem(1.336, 140), 'color', 'r')