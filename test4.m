addpath('functions/crpptbx-9.2.0/', 'functions/', 'functions/GPT');
load('data/coords.mat');
load('data/vessel.mat');
load('data/geometry_axuv.mat');
load('data/shot70777emissivity.mat');
load('data/shot70777axuvraw.mat');
load('data/transfer_matrix.mat');
mask = dlmread('data/mask.dat');

emission_at_t = shot70777emissivity.data(:,:,591)';

measurement = transfer_matrix * reshape(emission_at_t', 1875, 1);
params = optimize_parameters(measurement, [10000, 0.1], false);

reconstruction1 = perform_inversion(measurement, params, transfer_matrix, r_coords, z_coords, [], false);
reconstruction1(reconstruction1<0) = 0;
reconstruction1 = reconstruction1 .* flipud(mask);

reconstruction2 = gti_fisher(measurement, 0.004, transfer_matrix, 70777, 0.6, geometry);
reconstruction3 = gti_fisher(measurement, 0.04, [], 70777, 0.6, geometry);

subplot(141)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, emission_at_t)
colorbar;
title('Shot 70777 at 0.6s')
plot(r_vessel, z_vessel, 'w');

subplot(142)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, reconstruction1)
colorbar;
title('GPT')
plot(r_vessel, z_vessel, 'w');

subplot(143)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, reconstruction2)
colorbar;
title('Fisher my geometry')
plot(r_vessel, z_vessel, 'w');

subplot(144)
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, reconstruction3)
colorbar;
title('Fisher line geometry')
plot(r_vessel, z_vessel, 'w');


