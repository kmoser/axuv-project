addpath('functions/crpptbx-9.2.0/', 'functions/', 'functions/GPT');
load('data/coords.mat');
load('data/vessel.mat');
load('data/geometry_axuv.mat');
load('data/shot70777emissivity.mat');
load('data/shot70777axuvraw.mat');
load('data/transfer_matrix.mat');
mask = dlmread('data/mask.dat');
% 0.6 seconds
emission_at_t = shot70777emissivity.data(:,:,591)';
measurement = transfer_matrix * reshape(emission_at_t', 1875, 1);

[zi, ri] = find(emission_at_t == max(max(emission_at_t)));

r_max = r_coords(ri);
z_max = z_coords(zi);

% hold on;
% axis equal; set(gca,'YDir','Normal');
% imagesc(r_coords, z_coords, emission_at_t)
% colorbar;
% title('Shot 70777 at 0.6s')
% plot(r_vessel, z_vessel, 'w');
% scatter(r_max, z_max, 'r')
%tcv_polview('vtTPp',70777,shot70777emissivity.dim{3}(591)); % Overplot the mag equilib

% base_lengthscale = 0.05;
% added_lengthscale = 0.15;
% 
% local_lengthscale = zeros(size(emission_at_t));
% 
% for i = 1:size(local_lengthscale, 1)
%     z = z_coords(i);
%    for j = 1:size(local_lengthscale, 2)
%        r = r_coords(j);
%        dist = sqrt((r_max - r)^2 + (z_max - z)^2);
% 
%        local_lengthscale(i,j) = base_lengthscale + added_lengthscale/2 * (1 + tanh((dist-0.22)/0.12));
%    end
% end

% hold on;
% axis equal; set(gca,'YDir','Normal');
% imagesc(r_coords, z_coords, local_lengthscale)
% colorbar;
% %title('Shot 70777 at 0.6s')
% %plot(r_vessel, z_vessel, 'w');
% %scatter(r_max, z_max, 'r')

% kernel = zeros(size(r_coords, 1)*size(z_coords, 1));
% 
% for r1 = 1:size(r_coords)
%     for z1 = 1:size(z_coords)
%         
%         local_l1 = local_lengthscale(z1, r1);
%         
%         index1 = (z1-1)*length(r_coords) + r1;
% 
%         for r2 = 1:size(r_coords)size(r_coord
%             for z2 = 1:size(z_coords)
%                 
%                 local_l2 = local_lengthscale(z2, r2);
%                 
%                 index2 = (z2-1)*length(r_coords) + r2;
%                 sqdist = (r_coords(r1) - r_coords(r2))^2 + ...
%                         (z_coords(z1) - z_coords(z2))^2;
%                 kernel(index1, index2) = 150000^2 * ...
%                         exp(-sqdist/(local_l1^2 + local_l2^2));
% 
%             end
%         end  
%     end
% end

%params_non_stat = optimize_parameters(measurement, "non_stationary", [150000, 0.05, 0.15], false, emission_at_t);
params_non_stat(2)
params_non_stat(3)
%params_non_stat = [150000, 0.05, 0.15];

local_lengthscale = get_local_lengthscale(params_non_stat(2:3), r_max, z_max, r_coords, z_coords);
kernel = get_non_stationary_kernel([173000, 0.07, 0.1], emission_at_t, r_coords, z_coords);

kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
fit_non_stat = perform_inversion(measurement, [], transfer_matrix, r_coords, z_coords, kernel_inv, false);

fit_opt_l = perform_inversion(measurement, [150000, 0.128], transfer_matrix, r_coords, z_coords, [], false);
fit_small_l = perform_inversion(measurement, [150000, 0.05], transfer_matrix, r_coords, z_coords, [], false);
fit_big_l = perform_inversion(measurement, [150000, 0.2], transfer_matrix, r_coords, z_coords, [], false);

fit_non_stat(fit_non_stat < 0) = 0;
fit_small_l(fit_small_l < 0) = 0;
fit_big_l(fit_big_l < 0) = 0;
fit_opt_l(fit_opt_l < 0) = 0;

subplot(2, 3, 1);
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, emission_at_t)
colorbar;
title('Shot 70777 at 0.6s')
plot(r_vessel, z_vessel, 'w');
scatter(r_max, z_max, 'r', 'filled')
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot(2, 3, 2);
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, fit_non_stat .* flipud(mask))
colorbar;
plot(r_vessel, z_vessel, 'w');
title('Fit with local l')
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot(2, 3, 3);
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, local_lengthscale)
colorbar;
title('local length scale')
plot(r_vessel, z_vessel, 'black');
scatter(r_max, z_max, 'r', 'filled')
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot(2, 3, 4);
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, fit_opt_l .* flipud(mask))
colorbar;
title('Fit for l=10cm (optimal value)')
plot(r_vessel, z_vessel, 'w');
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot(2, 3, 5);
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, fit_small_l .* flipud(mask))
colorbar;
title('Fit for l=5cm')
plot(r_vessel, z_vessel, 'w');
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])

subplot(2, 3, 6);
hold on;
axis equal; set(gca,'YDir','Normal');
imagesc(r_coords, z_coords, fit_big_l .* flipud(mask))
colorbar;
title('Fit for l=20cm')
plot(r_vessel, z_vessel, 'w');
xlim([min(r_coords) max(r_coords)])
ylim([min(z_coords) max(z_coords)])