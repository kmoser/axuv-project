addpath('functions/crpptbx-9.2.0/');
addpath('functions/', 'functions/GPT/');
load('data/coords.mat');
load('data/vessel.mat');
load('data/shot70782emissivity.mat');
load('data/shot70782axuvraw.mat');
load('data/transfer_matrix.mat');
mask = dlmread('data/mask.dat');

stepsize = 2;
timebasis_axuv = shot70782axuvraw.time;
%axuv_raw_data = movmean(shot70782axuvraw.data, [stepsize, 0]);
axuv_raw_data = shot70782axuvraw.data;
emissivity = shot70782emissivity.data;
timebasis_emiss = shot70782emissivity.dim{3};

cal_data = calculate_calibration(   shot70782emissivity.data,   ...
                                    shot70782axuvraw.data,      ...
                                    timebasis_emiss, ...
                                    timebasis_axuv,      ...
                                    transfer_matrix,    ...
                                    false);

cal_data_at_t = cal_data(end, :);
axuv_start_index = round((1.497 + 0.04)/0.000005);
axuv_end_index = round((1.502 + 0.04)/0.000005);
time_interval = axuv_start_index:stepsize:axuv_end_index;
size(time_interval)

axuv_channel14_t_interval = axuv_raw_data(axuv_start_index:axuv_end_index, 14)*cal_data_at_t(14);

kernel = get_kernel([5000000 0.1], r_coords, z_coords);
kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));

inversions = zeros(size(time_interval,2), 75, 25);
inversions_var_l = zeros(size(time_interval,2), 75, 25);
measurements = zeros(size(time_interval,2), 140);
size(inversions)

for i = 1:size(time_interval, 2)
    time_index = time_interval(i);
    time = timebasis_axuv(time_index)
    measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
    measurements(i, :) = measurement;
    %e = gti_fisher(measurement', 0.05, T.transfer_matrix, 71157, time, geometry);
    e = perform_inversion(measurement', [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
    e(e<0) = 0;
    inversions(i, :, :) = e;
end

% for i = 1:size(time_interval, 2)
%     time_index = time_interval(i)
%     time = timebasis_axuv(time_index)
%     measurement = axuv_raw_data(time_index, :) .* cal_data_at_t;
%     kernel = get_non_stationary_kernel([3000000, 0.05, 0.15], reshape(inversions(i, :, :), 75, 25), r_coords, z_coords);
%     kernel = kernel + 0.001*diag(diag(kernel));
%     kernel_inv = inv(kernel + 0.001*diag(diag(kernel)));
%     e = perform_inversion(measurement', [], transfer_matrix, r_coords, z_coords, kernel_inv, false);
%     e(e<0) = 0;
%     inversions_var_l(i, :, :) = e;
% end

for i = 1:size(time_interval, 2)
    time = timebasis_axuv(time_interval(i));
    figure('Position',[10 10 800 600]);
    subplot(121)
    hold on;
    clims = [0 max(max(max(inversions)))];
    imagesc(r_coords, z_coords, reshape(inversions(i, :, :), 75, 25) .* flipud(mask), clims);
    axis equal; set(    gca,'YDir','Normal');
    plot(r_vessel, z_vessel, 'w', 'LineWidth', 2);
    xlabel('r in m'); ylabel('z in m');
    title(sprintf('70782 emissivity', time));
    c = colorbar;
    xlabel("R in m");
    ylabel("z in m");
    c.Label.String = ("W/m³");
    xlim([min(r_coords) max(r_coords)])
    ylim([min(z_coords) max(z_coords)])
    subplot(122)
    hold on;
    imagesc(1:140, timebasis_axuv(time_interval, :), axuv_raw_data(time_interval, :))
    plot(1:140, repelem(time, 140), 'color', 'r')
    title(sprintf('70782 AXUV t=%.4f', time));
    xlabel("channel");
    ylabel("time in is");
    xlim([1 140])
    ylim([timebasis_axuv(axuv_start_index) timebasis_axuv(axuv_end_index)])
    filename = sprintf('results/shot70782disruption/GPT_fixed_scale/emiss%7d.png', round(time*1000000));
    saveas(gcf, filename);
    close all;
end

%figure
%hold on;
%imagesc(r_coords, z_coords, reshape(inversions(1, :, :), 75, 25))
%axis equal; set(gca,'YDir','Normal');
%colorbar;
%itle('Emission_axuv')
%plot(r_vessel, z_vessel, 'w');

%imagesc(1:140, timebasis_axuv(time_interval, :), axuv_raw_data(time_interval, :))